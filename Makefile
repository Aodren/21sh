# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: abary <marvin@42.fr>                       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/11/23 12:31:57 by abary             #+#    #+#              #
#    Updated: 2016/11/04 16:07:02 by abary            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = 21sh

INC_DIR = includes

LIB_DIR = libft

INC_LIB_DIR = $(LIB_DIR)/$(INC_DIR)

NAME_LIB = lib21sh.a\

CFLAGS = -Wall -Werror -Wextra -I$(INC_DIR) -I $(INC_LIB_DIR) -g 
#CFLAGS = -I$(INC_DIR) -I $(INC_LIB_DIR)

SRC = main.c ft_init.c \
	  ft_destruct_shell.c ft_21sh.c       ft_debug.c \
	  ft_check_line.c ft_lstpbrk.c ft_search_variable.c 

SRC_EDITION = ft_add_ponctuation.c ft_clear_letter_delete.c\
			  ft_clear_letter_sup.c ft_clear_letter_delete_tab.c\
			  ft_search_key.c ft_reinit_line.c ft_create_buffer.c\
			  ft_calcul_position_cursor.c ft_sentence.c ft_read_input.c 

SRC_PATH =    ft_read_dir.c ft_init_path.c ft_recup_path.c ft_free_path.c


SRC_ACTIONS = ft_action_add.c ft_action_delete.c ft_action_enter.c\
			  ft_action_down.c ft_action_up.c ft_action_end.c ft_action_home.c\
			  ft_action_ctrl_v.c ft_action_tab.c ft_action_sup.c\
			  ft_action_backword.c ft_action_forword.c ft_action_ctrl_i.c\
			  ft_action_echap.c ft_action_right.c ft_action_left.c\
			  ft_action_copie_buffer.c ft_action_paste.c ft_action_couper_buffer.c\
			  ft_action_ctrl_h.c ft_action_ctrl_l.c 

SRC_TRIE = ft_init_trie.c ft_search_trie.c ft_create_trie.c ft_add_trie_node.c\
		   ft_free_trie.c ft_create_info.c ft_info_null.c

SRC_DISPLAY = ft_display_autocomplete_cmd.c ft_display.c ft_display_error.c

SRC_AUTO_DOSSIER = ft_handle_autocomplete_dossier.c\
				   ft_check_where_i_search.c ft_autocomplete.c\
				   ft_create_search_name.c ft_shell_valid.c ft_check_if_folder.c\
				   ft_add_autocomplete_folder.c ft_open_folder.c\
				   ft_add_folder_info.c


SRC_ENTER =  ft_enter_multi_lines.c \
			 ft_enter_single_line.c ft_enter_empty_line.c

SRC_TERMCAPS = ft_termios.c ft_termcaps_cursor.c ft_term_put.c\
			   ft_termcaps_key.c ft_termcaps_clear.c \
			   ft_access_db.c ft_calcul_size_fenetre.c ft_open_tty.c\
			   ft_termcaps_highlight.c ft_termcaps_init.c\
			   ft_termcaps_cursor_suite.c ft_termcaps_cursor_trilogie.c

SRC_ENV =	ft_add_env.c ft_free_env.c ft_getenv.c ft_create_env.c

SRC_HISTORY = ft_init.c ft_add_history.c

SRC_LEXER = ft_lexer.c ft_init_lexer.c ft_lexer_cmd.c ft_lexer_comment.c\
			ft_free_tokens.c ft_malloc_value.c ft_is_echapement.c\
			ft_double_quotes.c ft_replace_variables.c\
			ft_lexer_arg.c ft_lexer_value.c ft_lexer_equal.c ft_lexer_and.c\
			ft_lexer_background.c ft_lexer_pipe.c ft_lexer_or.c\
			ft_lexer_popen.c ft_lexer_pclose.c ft_lexer_mquotes.c\
			ft_lexer_end.c ft_lexer_files.c\
			ft_lexer_more_than.c ft_lexer_agregation.c\
			ft_lexer_less_than.c ft_lexer_whitespaces.c\
			ft_lexer_backslash.c ft_open_brackets.c\
			ft_close_brackets.c ft_lexer_d_less_than.c \
			ft_lexer_d_more_than.c ft_lexer_end_of_files.c\
			ft_strdupequ.c ft_display_token.c ft_single_quotes.c\
			ft_shell_home.c

SRCS = $(addprefix sources/,$(SRC))\
	   $(addprefix sources/trie/,$(SRC_TRIE))\
	   $(addprefix sources/display/,$(SRC_DISPLAY))\
	   $(addprefix sources/auto_dossier/,$(SRC_AUTO_DOSSIER))\
	   $(addprefix sources/actions/,$(SRC_ACTIONS))\
	   $(addprefix sources/enter/,$(SRC_ENTER))\
	   $(addprefix sources/termcaps/,$(SRC_TERMCAPS))\
	   $(addprefix sources/env/,$(SRC_ENV))\
	   $(addprefix sources/history/,$(SRC_HISTORY))\
	   $(addprefix sources/edition/,$(SRC_EDITION))\
	   $(addprefix sources/path/,$(SRC_PATH))\
	   $(addprefix sources/lexer/,$(SRC_LEXER))

OBJ = $(SRCS:.c=.o)

CC = gcc

all : $(NAME)

$(NAME) : $(OBJ)
	(cd $(LIB_DIR) && $(MAKE))
	ar -r $(NAME_LIB) $(OBJ)
	#	gcc -o $(NAME) $(NAME_LIB) libft/libft.a  -ltermcap -lncurses -ltcl8.5
	gcc -o $(NAME) $(NAME_LIB) libft/libft.a  -ltermcap -lncurses

clean :
	(cd $(LIB_DIR) && make clean && cd ..)
	rm -rf $(OBJ)

fclean : clean
	(cd $(LIB_DIR) && make fclean && cd ..)
	rm -rf $(NAME)
	rm -rf $(NAME_LIB)

re : fclean all

.PHONY: all clean fclean re
