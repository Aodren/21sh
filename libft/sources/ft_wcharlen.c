#include <wchar.h>
#include "libft.h"
/*
** Renvoie le nombre de caratere imprime par une chaine et non le
** nombre d'octet que contient la chaine
*/

size_t		ft_wcharlen(const wchar_t str)
{
	size_t nbr = 0;

	ft_putchar('\n');
	if ((str & (~0x7f))  == 0)
		return (1);
	else if ((str & (~0x7FF)) == 0)
		return (2);
	else if ((str & (~0xFFFF)) == 0)
		return (3);
	else
		return (4);
}
