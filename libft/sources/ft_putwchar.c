#include <unistd.h>
#include "libft.h"
#include <wchar.h>

#include <stdio.h>
void	ft_putwchar(const wchar_t *str)
{
	unsigned int			nbr;
	unsigned char			d;

	while (*str)
	{
		nbr = ft_wcharlen(*str);
		if (nbr == 1)
			write(1, str, 1);
		else if (nbr == 2)
		{
			d = ((0xdfff & (*str << 2)) >> 8) | 0xc0;
			write(1, &d, 1);
			d = (0xbf & *str) | 0x80;
			write(1, &d, 1);
		}
		else if (nbr == 3)
		{
			d = ((0xefffff & (*str << 4)) >> 16) | 0xe0;
			write(1, &d, 1);
			d = ((0xbfff & *str << 2) >> 8) | 0x80;
			write(1, &d, 1);
			d = (0xbf & *str) | 0x80;
			write(1, &d, 1);
			wprintf(L" 3  octets : %lc\n", *str);
		}
		else if (nbr == 4)
		{
			d = ((0xf7ffffff & (*str << 6)) >> 24) | 0xf0;
			write(1, &d, 1);
			d = ((0xbfffff & *str << 4) >> 16) | 0x80;
			write(1, &d, 1);
			d = ((0xbfff & *str << 2) >> 8) | 0x80;
			write(1, &d, 1);
			d = (0xbf & *str) | 0x80;
			write(1, &d, 1);
			wprintf(L" 4  octets : %lc\n", *str);
		}
		++str;
	}
}
