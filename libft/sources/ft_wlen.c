#include "libft.h"
/*
** Renvoie le nombre de caratere imprime par une chaine et non le
** nombre d'octet que contient la chaine
*/

size_t		ft_wlen(const char *str)
{
	size_t nbr = 0;

	while (*str)
	{
		if ((*str & 0xF0) == 0xF0)
			str += 4;
		else if ((*str & 0xE0) == 0XE0)
			str += 3;
		else if ((*str & 0xC0) == 0XC0)
			str += 2;
		else
			++str;
		++nbr;
	}
	return (nbr);
}
