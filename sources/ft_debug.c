#include "libft.h"
#include "ft_shell.h"

/*
void	ft_display_list(t_sent *sent)
{
	while (sent)
	{
		ft_putchar(sent->input);
		sent = sent->next;
	}
}
*/

void	ft_test_reverse(t_sent *sent)
{
	while (sent->next)
		sent = sent->next;
	while (sent)
	{
		ft_putchar(sent->input);
		sent = sent->prev;
	}
}
