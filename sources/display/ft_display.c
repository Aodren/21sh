#include "libft.h"
#include "ft_shell.h"

void	ft_display_list(t_sent *sent, int actions)
{
	while (sent)
	{
		if (actions != COUPER)
		{
			if (sent->highlight)
				ft_termcaps_highlight_on();
			ft_putchar(sent->input);
			if (sent->highlight)
				ft_termcaps_highlight_off();
		}
		else
		{
			ft_putchar(sent->input);
			sent->highlight = 0;
		}
		sent = sent->next;
	}
}

void		ft_display_reput_line(t_21sh *sh, int action)
{
	unsigned int y;

	y = sh->cursor_y;
	ft_termcaps_cursor_begin();
	while (y)
	{
		ft_termcaps_cursor_up();
		--y;
	}
	ft_termcaps_clear_down();
	if (IS_MULTI_LINE(sh->ponctuations))
		ft_putchar('>');
	else
		ft_putstr(sh->prompt);
	ft_display_list(sh->begin_line, action);
	if (sh->cursor_x == 0 && !sh->focus && !sh->n_presence)
		ft_termcaps_cursor_down();
	y = sh->y_line;
	while (y > sh->cursor_y)
	{
		ft_termcaps_cursor_up();
		--y;
	}
	ft_termcaps_cursor_nright(sh->cursor_x);
}

void	ft_display_history(unsigned int y, t_21sh *sh)
{
	ft_termcaps_cursor_begin();
	while (y)
	{
		ft_termcaps_cursor_up();
		--y;
	}
	ft_termcaps_clear_down();
	ft_putstr(sh->prompt);
	ft_display_list(sh->begin, 0);
}
