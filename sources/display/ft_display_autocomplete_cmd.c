#include "ft_shell.h"
#include "libft.h"
#include <stdlib.h>

static char		**ft_create_tab_autocomplete(unsigned int *tab,
		t_cmd_info *info, unsigned int size_max)
{
	char			**affiche;
	unsigned int	i;
	unsigned int	d;

	affiche = ft_memalloc(sizeof(char *) * (tab[1] + 1));
	d = 0;
	while (d < tab[1])
	{
		affiche[d] = ft_memalloc(sizeof(char) * ((size_max + 1) * tab[0] + 1));
		ft_memset(affiche[d], ' ', (size_max + 1) * tab[0]);
		++d;
	}
	d = 0;
	i = 0;
	while (info)
	{
		ft_memcpy(affiche[d] + i, info->name, info->size);
		++d;
		if (d == tab[1] && !(d = 0))
			i += info->size + (size_max - info->size + 1);
		info = info->next;
	}
	return (affiche);
}

void		ft_display_autocomplete_cmd(t_cmd_info *info, t_21sh *sh,
		size_t size_max, unsigned int nbr)
{
	unsigned int	tab[2];
	char			**affiche;

	tab[0] = (sh->winsize_x / (size_max + 1));
	if (tab[0] > nbr)
		tab[0] = nbr;
	if (!tab[0])
		tab[0] = 1;
	if (tab[0] == nbr)
		tab[1] = 1;
	else
		tab[1] = (nbr / tab[0]) + 1;
	affiche = ft_create_tab_autocomplete(tab, info, size_max);
	tab[0] = 0;
	while (tab[0] < tab[1])
	{
		ft_putendl(affiche[tab[0]]);
		free(affiche[tab[0]]);
		affiche[tab[0]] = 0;
		tab[0]++;
	}
	free(affiche);
	affiche = 0;
}
