#include "ft_shell.h"
#include "libft.h"
#include <stdlib.h>

void	ft_display_error(int error)
{
	if (error == MALLOC)
		ft_putendl_fd("erreur malloc", 2);
	if (error == TERMIOS_ERROR)
		ft_putendl_fd("erreur set/get termios", 2);
	if (error == TTY)
		ft_putendl_fd("erreur recup tty name", 2);
	if (error == ENV_TERM)
		ft_putendl_fd("variable term introuvable", 2);
	if (error == DATABASE_FOUND)
		ft_putendl_fd("databse not found", 2);
	if (error == ENV_TERM)
		ft_putendl_fd("term incompatible", 2);
	exit(EXIT_FAILURE);
}
