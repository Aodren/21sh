#include <stdlib.h>

void	ft_free_env(char **env)
{
	char **d;

	d = env;
	while (*env)
	{
		free(*env++);
	}
	free(d);
}
