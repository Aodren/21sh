#include "libft.h"
#include "ft_shell.h"

void	ft_create_env(t_21sh *sh, char **env)
{
	if (*env)
	{
		while (*env)
			ft_add_env(sh, *env++);
		ft_init_path(sh);
		return ;
	}
}
