#include "libft.h"
#include "ft_shell.h"
#include <stdlib.h>

static	void	ft_free_old_env(t_21sh *sh)
{
	char **old_env;

	old_env = sh->env;
	free (old_env);
	sh->env = NULL;
}


static char	**ft_copy_env(t_21sh *sh, char *var, char **new_env)
{
	char	**old_env;
	char	**d;

	old_env = sh->env;
	d = new_env;
	if (!old_env)
	{
		*new_env = ft_strdup(var);
		return (new_env);
	}
	while (*old_env && ft_strcmp(*old_env, var) < 0)
		*new_env++ = *old_env++;
	*new_env++ = ft_strdup(var);
	while (*old_env)
		*new_env++ = *old_env++;
	return (d);
}

void	ft_add_env(t_21sh *sh, char *var)
{
	char **new_env;

	sh->size_env++;
	if (!(new_env = ft_memalloc(sizeof(char *) * (sh->size_env + 1))))
		ft_display_error(MALLOC);
	new_env = ft_copy_env(sh, var, new_env);
	new_env[sh->size_env] = NULL;
	ft_free_old_env(sh);
	sh->env = new_env;
}
