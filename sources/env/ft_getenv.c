#include "libft.h"

/*
** recupere la valeur d'une variable env
*/

char	*ft_getenv(char *var, char **env)
{
	(void)var;
	while (*env)
	{
		if (ft_strchrstart(*env, var))
			return (ft_strchr(*env, '=') + 1);
		++env;
	}
	return ((void*)0);
}
