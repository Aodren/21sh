#include "ft_shell.h"
#include "libft.h"

int		ft_enter_single_line(t_21sh *sh)
{
	sh->focus_history = 0;
	ft_add_history(sh);
	ft_putchar('\n');
	ft_display_list(sh->begin, 0);
	ft_reinit_line(sh);
	ft_putstr(sh->prompt);
	//ft_termcaps_save_position();
	return (1);
}
