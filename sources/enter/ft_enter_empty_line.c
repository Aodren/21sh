#include "libft.h"
#include "ft_shell.h"

int		ft_enter_empty_line(t_21sh *sh)
{
	ft_putchar('\n');
	ft_reinit_line(sh);
	ft_putstr(sh->prompt);
	return (0);
}
