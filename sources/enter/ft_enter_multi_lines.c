#include "ft_shell.h"
#include "libft.h"

int		ft_enter_multi_lines(t_21sh *sh)
{
	ft_add_sentence('\n', sh, 0);
	if (sh->focus)
		ft_putchar('\n');
	MULTI_LINES(sh->ponctuations);
	sh->begin_line = (void *)0;
	sh->focus = (void *)0;
	sh->cursor_x = 0;
	sh->cursor_y++;
	ft_putchar('>');
	return (1);
}
