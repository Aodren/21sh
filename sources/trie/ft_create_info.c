#include "ft_trie.h"
#include "libft.h"

void	ft_create_list_info(t_cmd *root, t_21sh *sh, size_t *s_max,
		unsigned int *nbr)
{
	if (root)
	{
		if (root->info)
		{
			++*nbr;
			*s_max = root->info->size > *s_max ? root->info->size : *s_max;
			root->info->next = 0;
			if (!sh->begin_info)
			{
				sh->begin_info = root->info;
				sh->end_info = root->info;
			}
			else
			{
				sh->end_info->next = root->info;
				sh->end_info = root->info;
			}
		}
		ft_create_list_info(root->child, sh, s_max, nbr);
		ft_create_list_info(root->next, sh, s_max, nbr);
	}
}
