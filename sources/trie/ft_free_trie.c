#include "ft_shell.h"
#include <stdlib.h>

void			ft_free_trie(t_cmd *root)
{
	if (root)
	{
		ft_free_trie(root->child);
		ft_free_trie(root->next);
		if (root->info)
		{
			free(root->info->name);
			if (root->info->path)
				free(root->info->path);
			free(root->info);
		}
		free(root);
	}
}
