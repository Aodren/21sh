#include "ft_shell.h"
#include "libft.h"

t_cmd_info	*ft_init_info(char *name, char *path)
{
	t_cmd_info *info;

	if(!(info = (t_cmd_info *)ft_memalloc(sizeof(t_cmd_info))))
		ft_display_error(MALLOC);
	info->name = ft_strdup(name);
	if (path)
		info->path = ft_strdup(path);
	else
		info->path = (void *)0;
	info->next =  (void *)0;
info->size = ft_strlen(name);
//	info->size = ft_wlen(name);
	return (info);
}

/*
** Init du root et des leafs
*/

t_cmd	*ft_init_cmd_root(void)
{
	t_cmd *root;

	if(!(root = (t_cmd *)ft_memalloc(sizeof(t_cmd))))
		ft_display_error(MALLOC);
	root->child = (void *)0;
	root->next = (void *)0;
	root->letter = 0;
	root->info = (void *)0;
	return (root);
}

// Il faut supprimer cmd->cmd et cmd->nom
t_cmd	*ft_init_node(char *name, char *path, char *nom)
{
	t_cmd *node;

	if(!(node = (t_cmd *)ft_memalloc(sizeof(t_cmd))))
		ft_display_error(MALLOC);
	node->letter = *name;
	node->child = (void *)0;
	node->next = (void *)0;
	if (*(name + 1))
	{
		node->info = 0;
		return (node);
	}
	// il faut init cmd info ici
	node->info = ft_init_info(nom, path);
	return (node);
}
