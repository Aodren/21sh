#include "ft_struct.h"
#include <stdlib.h>

/*
** Remise a zero des pointeurs info->next
*/

void	ft_info_null(t_cmd_info *begin)
{
	if (begin)
	{
		ft_info_null(begin->next);
		begin->next = 0;
	}
}

#include "libft.h"
void	ft_free_info(t_cmd_info *begin)
{
	if (begin)
	{
		ft_free_info(begin->next);
		if (begin->name)
		{
			free (begin->name);
		}
		if (begin->path)
			free (begin->path);
	//	begin->next = 0;
		free(begin);
	}
}
