#include "ft_shell.h"

/*
** Renvoie le maillon correspondant a la commande taper
*/


#include "libft.h"

t_cmd	*ft_search_commande(t_cmd *root, char *name, char *nom)
{

	if (!*name)
		return (root);
	if (root)
	{
		while (root && root->letter != *name)
			root = root->next;
		if (root)
		{
			++name;
			if (!*name)
				return root;
			return  ft_search_commande(root->child, name, nom);
		}
	}
	return 0;
}

void	ft_parcour_trie(t_cmd *root)
{

	if (root)
	{
		if (root->info)
		{
			ft_putendl(root->info->name);
		}
			ft_parcour_trie(root->child);
			ft_parcour_trie(root->next);
	}
}
