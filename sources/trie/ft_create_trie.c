#include "ft_shell.h"
#include <dirent.h>
#include "libft.h"
#include "ft_trie.h"

void	ft_create_trie(t_21sh *sh, struct dirent *dp, char *path)
{
	if (!sh->cmd_root)
	{
		 sh->cmd_root = ft_init_cmd_root();
		 ft_no_child(sh->cmd_root, path, dp->d_name, dp->d_name);

	}
	else
		ft_add_trie_node(sh->cmd_root, path, dp->d_name, dp->d_name);
}
