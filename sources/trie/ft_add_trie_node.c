#include <dirent.h>
#include "ft_trie.h"
#include "ft_shell.h"
#include "libft.h"

void ft_no_child(t_cmd *cmd, char *path, char *name, char *nom)
{
	while (*name)
	{
		cmd->child = ft_init_node(name, path, nom);
		cmd = cmd->child;
		++name;
	}
}

void	ft_add_trie_node(t_cmd *cmd, char *path, char *name, char *nom)
{
	t_cmd *tmp;
	t_cmd *tmp2;

	if (*name)
	{
		if (!cmd->child)
			ft_no_child(cmd, path, name, nom);
		else
		{
			tmp = cmd->child;
			while (tmp && tmp->letter != *name)
					tmp = tmp->next;
			if (!tmp)
			{
				tmp2 = (void *)0;
				tmp = cmd->child;
				while (tmp && tmp->letter <= *name)
				{
					tmp2 = tmp;
					tmp = tmp->next;
				}
				if (!tmp2)
				{
					cmd->child = ft_init_node(name++, path, nom);
					cmd->child->next = tmp; // faire une trie
					ft_no_child(cmd->child, path, name, nom);
				}
				else
				{
					tmp2->next = ft_init_node(name++, path, nom);
					tmp2->next->next = tmp;
					ft_no_child(tmp2->next, path, name, nom);
				}
			}
			else
			{
				if (tmp->letter == *name && !*(name + 1) && !tmp->info)
					tmp->info = ft_init_info(nom, path);
				else if (!tmp->child)
					ft_no_child(tmp, path, ++name, nom);
				else
					ft_add_trie_node(tmp, path, ++name, nom);
			}
		}
	}
}
