#include "libft.h"
#include "ft_shell.h"

int		ft_sh_paste(t_21sh *sh)
{
	t_sent			*buffer;
	unsigned int	nbr;

	ft_calcul_pos_cursor(sh);
	nbr = sh->cursor_y;
	buffer = sh->buffer;
	while (buffer)
	{
		ft_add_sentence(buffer->input, sh, buffer->tab);
		buffer = buffer->next;
	}
	if (!sh->focus)
		return (1);
	ft_calcul_pos_cursor(sh);
	while (sh->cursor_y > nbr)
	{
		ft_termcaps_cursor_down();
		++nbr;
	}
	ft_display_reput_line(sh, COUPER);
	return (1);
}
