#include "libft.h"
#include "ft_shell.h"

int		ft_sh_home(t_21sh *sh)
{
	unsigned int	y;

	if (sh->focus == sh->begin_line)
		return (0);
	ft_calcul_pos_cursor(sh);
	y = sh->cursor_y;
	while (y > 0)
	{
		ft_termcaps_cursor_up();
		--y;
	}
	sh->focus = sh->begin_line;
	if (IS_MULTI_LINE(sh->ponctuations))
		ft_termcaps_cursor_nright(1);
	else
		ft_termcaps_cursor_nright(ft_strlen(sh->prompt));
	return (1);
}
