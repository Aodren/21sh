#include "ft_shell.h"
#include "libft.h"

static t_sent	*ft_sh_left_tab(t_sent *focus, t_21sh *sh, unsigned int *tab)
{
	unsigned short i;

	if (focus->tab)
	{
		i = 7;
		*tab = 1;
		if (sh->last_action == 0)
			sh->focus->next->highlight =
				!sh->focus->next->highlight;
		while (i)
		{
			if (sh->insertion)
				focus->highlight = !focus->highlight;
			focus = focus->prev;
			--i;
		}
	}
	return (focus);
}

static void		ft_sh_left_insertion(t_21sh *sh, unsigned int tab)
{
	if (!tab && sh->last_action == 0 && sh->focus->next)
		sh->focus->next->highlight =
			!sh->focus->next->highlight;
	ft_calcul_pos_cursor(sh);
	sh->focus->highlight = !sh->focus->highlight;
	ft_display_reput_line(sh, LEFT);
	sh->last_action = 1;
}

static void		ft_sh_left_cursor_right(t_21sh *sh)
{
	ft_termcaps_cursor_up();
	ft_calcul_pos_cursor(sh);
	ft_termcaps_cursor_nright(sh->cursor_x);
}

int				ft_sh_left(t_21sh *sh)
{
	unsigned int nbr;
	unsigned int tab;

	if (sh->focus != sh->begin_line)
	{
		tab = 0;
		ft_calcul_pos_cursor(sh);
		nbr = sh->cursor_y;
		sh->focus = sh->focus != 0 ? sh->focus->prev : sh->end;
		sh->focus = ft_sh_left_tab(sh->focus, sh, &tab);
		if (sh->cursor_x == 0)
			ft_sh_left_cursor_right(sh);
		else
		{
			ft_calcul_pos_cursor(sh);
			if (nbr > sh->cursor_y)
				ft_termcaps_cursor_up();
			ft_termcaps_cursor_nright(sh->cursor_x);
		}
		if (sh->insertion && sh->focus)
			ft_sh_left_insertion(sh, tab);
	}
	return (1);
}
