#include "ft_shell.h"
#include "libft.h"

int			ft_sh_add_cara(t_21sh *sh)
{
	unsigned int y;

	y = sh->cursor_y;
	ft_calcul_pos_cursor(sh);
	if (y < sh->cursor_y)
		ft_termcaps_cursor_down();
	ft_display_reput_line(sh, ADD_CARA);
	return (1);
}
