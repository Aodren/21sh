
#include "libft.h"
#include "ft_shell.h"

int		ft_action_copie_buffer(t_21sh *sh)
{
	t_sent *tmp;

	tmp = sh->begin;
	if (sh->buffer)
	{
		ft_clear_sentence(sh->buffer);
		sh->buffer = 0;
	}
	ft_create_buffer(sh);
	ft_sh_echap(sh);
	return (1);
}
