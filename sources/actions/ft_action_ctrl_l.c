#include "libft.h"
#include "ft_shell.h"

int	ft_action_ctrl_l(t_21sh *sh)
{
	unsigned int y;

	ft_calcul_pos_cursor(sh);
	if (sh->cursor_y == sh->y_line)
		return (0);
	y = sh->cursor_y;
	sh->focus = ft_focus_up(sh->begin_line, sh->cursor_y + 1, sh,
			CTRL_L);
	ft_calcul_pos_cursor(sh);
	while (y < sh->cursor_y)
	{
		ft_termcaps_cursor_down();
		++y;
	}
	ft_termcaps_cursor_nright(sh->cursor_x);
	return (1);
}
