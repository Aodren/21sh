#include "libft.h"
#include "ft_shell.h"

static t_sent	*ft_sh_forword_on_word(t_sent *tmp)
{
	while (tmp->next && !ft_isspace(tmp->next->input))
		tmp = tmp->next;
	return (tmp);
}

int				ft_sh_forword(t_21sh *sh)
{
	t_sent			*tmp;
	unsigned int	y;

	if (!sh->focus || !sh->end || !sh->focus->next)
		return (0);
	ft_calcul_pos_cursor(sh);
	y = sh->cursor_y;
	tmp = sh->focus;
	if (!ft_isspace(tmp->next->input))
		sh->focus = ft_sh_forword_on_word(tmp);
	tmp = tmp->next;
	ft_termcaps_cursor_right();
	while (tmp && ft_isspace(tmp->input))
	{
		ft_termcaps_cursor_right();
		tmp = tmp->next;
	}
	if (tmp)
		sh->focus = ft_sh_forword_on_word(tmp);
	ft_calcul_pos_cursor(sh);
	while (++y < sh->cursor_y + 1)
		ft_termcaps_cursor_down();
	ft_termcaps_cursor_nright(sh->cursor_x);
	return (1);
}
