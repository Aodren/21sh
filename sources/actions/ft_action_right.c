#include "ft_shell.h"
#include "libft.h"

static t_sent	*ft_sh_left_tab(t_sent *focus, t_21sh *sh, unsigned int *tab)
{
	unsigned short i;

	if (focus && focus->tab)
	{
		if (!focus->prev->tab)
			i = 8;
		else
			i = 7;
		*tab = 1;
		if (sh->last_action == 1)
			sh->focus->prev->highlight =
				!sh->focus->prev->highlight;
		while (i)
		{
			if (sh->insertion)
				focus->highlight = !focus->highlight;
			focus = focus->next;
			--i;
		}
	}
	return (focus);
}

int				ft_sh_right(t_21sh *sh)
{
	unsigned int y;
	unsigned int tab;

	if (sh->focus)
	{
		tab = 0;
		ft_calcul_pos_cursor(sh);
		y = sh->cursor_y;
		sh->focus = sh->focus->next;
		sh->focus = ft_sh_left_tab(sh->focus, sh, &tab);
		ft_calcul_pos_cursor(sh);
		if (sh->cursor_x == 0 || y < sh->cursor_y)
			ft_termcaps_cursor_down();
		else
			ft_termcaps_cursor_nright(sh->cursor_x);
		if (sh->insertion && sh->focus)
		{
			if (!tab && sh->last_action == 1)
				sh->focus->prev->highlight = !sh->focus->prev->highlight;
			sh->focus->highlight = !sh->focus->highlight;
			sh->last_action = 0;
			ft_display_reput_line(sh, RIGHT);
		}
	}
	return (1);
}
