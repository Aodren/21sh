#include "libft.h"
#include "ft_shell.h"
#include <stdlib.h>

static unsigned int	ft_calcul_nbr_highlight(t_21sh *sh)
{
	unsigned int	nbr;
	t_sent			*tmp;

	nbr = 0;
	tmp = sh->begin;
	while (tmp)
	{
		if (tmp->highlight)
			++nbr;
		tmp = tmp->next;
	}
	return (nbr);
}

static void			ft_reaffiche_line_after_cut(t_21sh *sh, unsigned int nbr)
{
	nbr = sh->cursor_y;
	ft_calcul_pos_cursor(sh);
	while (nbr > sh->cursor_y)
	{
		ft_termcaps_cursor_up();
		--nbr;
	}
	ft_display_reput_line(sh, COUPER);
	sh->insertion = 0;
}

int					ft_action_couper_buffer(t_21sh *sh)
{
	t_sent			*tmp;
	unsigned int	nbr;

	ft_calcul_pos_cursor(sh);
	if (sh->buffer)
	{
		ft_clear_sentence(sh->buffer);
		sh->buffer = 0;
	}
	ft_create_buffer(sh);
	nbr = ft_calcul_nbr_highlight(sh);
	tmp = sh->focus;
	sh->focus = sh->begin;
	while (sh->focus && !sh->focus->highlight)
		sh->focus = sh->focus->next;
	while (nbr > 0)
	{
		if ((sh->focus && sh->focus->tab) || sh->end->tab)
			nbr = nbr - 7;
		ft_clear_letter_sup(sh);
		--nbr;
	}
	ft_reaffiche_line_after_cut(sh, nbr);
	return (1);
}
