#include "ft_shell.h"
#include "libft.h"

int		ft_sh_ctrl_i(t_21sh *sh)
{
	if (sh->begin && !sh->insertion)
	{
		sh->insertion = 1;
		sh->focus_insertion = sh->focus == 0 ? sh->end : sh->focus;
		if (!sh->focus)
			sh->focus = sh->end;
		sh->focus_insertion->highlight = 1;
		ft_calcul_pos_cursor(sh);
		ft_display_reput_line(sh, ADD_CARA);
	}
	return (1);
}
