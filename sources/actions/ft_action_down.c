#include "ft_shell.h"
#include "libft.h"
#include <stdlib.h>

void	ft_put_history(t_21sh *sh)
{
	char			*d;
	unsigned int	y;

	d = sh->focus_history->line;
	while (*d)
	{
		if (*d == '\t')
		{
			y = 8;
			while (y)
			{
				ft_add_sentence(' ', sh, 1);
				--y;
			}
			++d;
			if (!*d)
				break ;
		}
		ft_add_sentence(*d, sh, 0);
		++d;
	}
}

int		ft_sh_down(t_21sh *sh)
{
	unsigned int y;

	if (!sh->focus_history)
		return (0);
	free(sh->focus_history->line);
	sh->focus_history->line = ft_create_new_history(sh);
	sh->focus_history = sh->focus_history->next;
	ft_calcul_pos_cursor(sh);
	y = sh->cursor_y;
	ft_termcaps_cursor_begin();
	while (y)
	{
		ft_termcaps_cursor_up();
		--y;
	}
	ft_termcaps_clear_down();
	ft_putstr(sh->prompt);
	ft_reinit_line(sh);
	if (sh->focus_history)
		ft_put_history(sh);
	return (1);
}
