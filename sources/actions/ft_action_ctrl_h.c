#include "libft.h"
#include "ft_shell.h"

static unsigned int	ft_s_promt(unsigned int prompt, t_21sh *sh)
{
	unsigned int nbr;

	if (IS_MULTI_LINE(sh->ponctuations))
		nbr = 1;
	else
		nbr = prompt;
	return (nbr);
}

t_sent				*ft_focus_last_line(t_sent *begin, t_21sh *sh,
		unsigned int tab, unsigned int nbr)
{
	while (begin)
	{
		if (nbr >= sh->cursor_x || begin->input == '\n' || nbr == sh->winsize_x)
			break ;
		++nbr;
		if (begin->tab)
			++tab;
		if (tab == 8)
			tab = 0;
		begin = begin->next;
	}
	while (begin && begin->tab && (tab < 8 && tab > 0))
	{
		if (tab < 4)
		{
			begin = begin->prev;
			--tab;
		}
		else
		{
			begin = begin->next;
			++tab;
		}
	}
	return (begin);
}

t_sent				*ft_focus_up(t_sent *begin, unsigned int cursor_y,
		t_21sh *sh, int action)
{
	unsigned int nbr;
	unsigned int tab;

	nbr = ft_s_promt(ft_strlen(sh->prompt) + 1, sh);
	while (begin && cursor_y)
	{
		if (begin->input == '\n' || nbr == sh->winsize_x)
		{
			nbr = 0;
			cursor_y--;
		}
		++nbr;
		begin = begin->next;
	}
	if ((sh->cursor_y - 1 && action == CTRL_H) || action == CTRL_L)
		nbr = ft_s_promt(0, sh);
	else
		nbr = ft_s_promt(ft_strlen(sh->prompt), sh);
	tab = 0;
	return (ft_focus_last_line(begin, sh, tab, nbr));
}

int					ft_action_ctrl_h(t_21sh *sh)
{
	unsigned int y;

	ft_calcul_pos_cursor(sh);
	if (sh->cursor_y == 0)
		return (0);
	y = sh->cursor_y;
	sh->focus = ft_focus_up(sh->begin_line, sh->cursor_y - 1, sh, CTRL_H);
	ft_calcul_pos_cursor(sh);
	while (y > sh->cursor_y)
	{
		ft_termcaps_cursor_up();
		--y;
	}
	ft_termcaps_cursor_nright(sh->cursor_x);
	return (1);
}
