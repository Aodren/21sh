#include "ft_shell.h"
#include "libft.h"

static void	ft_display_after_focus(t_21sh *sh)
{
	t_sent	*tmp;

	tmp = sh->focus;
	ft_termcaps_cursor_begin();
	ft_termcaps_save_position();
	ft_termcaps_clear_line();
	while (tmp)
	{
		ft_putchar(tmp->input);
		tmp = tmp->next;
	}
}

int			ft_sh_sup_cara(t_21sh *sh)
{
	unsigned int nbr;

	if (sh->begin)
	{
		ft_calcul_pos_cursor(sh);
		nbr = sh->cursor_y;
		if (ft_clear_letter_sup(sh))
		{
			ft_calcul_pos_cursor(sh);
			if (sh->cursor_x == 0)
			{
				ft_display_after_focus(sh);
				ft_termcaps_old_position();
			}
			else
			{
				if (nbr > sh->cursor_y)
					ft_termcaps_cursor_up();
				ft_display_reput_line(sh, DELETE);
			}
			return (1);
		}
	}
	return (0);
}
