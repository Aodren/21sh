#include "ft_shell.h"
#include "libft.h"
#include <stdlib.h>

static void		ft_sh_up_display(t_21sh *sh)
{
	unsigned int y;

	ft_calcul_pos_cursor(sh);
	y = sh->cursor_y + 1;
	ft_termcaps_cursor_begin();
	while (--y)
		ft_termcaps_cursor_up();
	ft_termcaps_clear_down();
	ft_putstr(sh->prompt);
	ft_reinit_line(sh);
	ft_put_history(sh);
}

int				ft_sh_up(t_21sh *sh)
{
	if (!sh->begin_history)
		return (0);
	if (sh->focus_history)
	{
		if (!sh->focus_history->prev)
			return (0);
		free(sh->focus_history->line);
		sh->focus_history->line = ft_create_new_history(sh);
	}
	else if (ft_check_no_empty_line(sh))
		ft_add_history(sh);
	if (sh->focus_history)
		sh->focus_history = sh->focus_history->prev;
	else
		sh->focus_history = sh->end_history;
	ft_sh_up_display(sh);
	return (1);
}
