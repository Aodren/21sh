#include "ft_shell.h"
#include "libft.h"

int		ft_sh_tab(t_21sh *sh)
{
	unsigned int i;

	if (IS_CTRL_V(sh->parse))
	{
		i = 8;
		while (i)
		{
			ft_add_sentence(' ', sh, 1);
			--i;
		}
		CTRL_V(sh->parse);
		if (sh->focus)
			ft_sh_add_cara(sh);
	}
	else
		ft_autocomplete(sh);
	return (1);
}
