#include "ft_shell.h"
#include "libft.h"

int		ft_sh_delete_cara(t_21sh *sh)
{
	unsigned int nbr;

	if (sh->begin)
	{
		nbr = sh->cursor_y;
		if (ft_clear_letter_delete(sh))
		{
			ft_calcul_pos_cursor(sh);
			if (nbr > sh->cursor_y)
				ft_termcaps_cursor_up();
			ft_display_reput_line(sh, DELETE);
		}
	}
	return (0);
}
