#include "libft.h"
#include "ft_shell.h"

t_sent		*ft_sh_backword_on_word(t_sent *tmp, t_21sh *sh)
{
	while (tmp != sh->begin_line && tmp->prev && !ft_isspace(tmp->prev->input))
		tmp = tmp->prev;
	return (tmp);
}

static void	ft_move_cursor_backword(t_21sh *sh, unsigned int y)
{
	ft_calcul_pos_cursor(sh);
	while (y > sh->cursor_y)
	{
		ft_termcaps_cursor_up();
		--y;
	}
	ft_termcaps_cursor_nright(sh->cursor_x);
}

int			ft_sh_backword(t_21sh *sh)
{
	t_sent			*tmp;
	unsigned int	y;

	tmp = sh->focus == 0 ? sh->end : sh->focus;
	if (tmp == sh->begin_line || !tmp)
		return (0);
	ft_calcul_pos_cursor(sh);
	y = sh->cursor_y;
	if (!ft_isspace(tmp->prev->input))
		sh->focus = ft_sh_backword_on_word(tmp, sh);
	else if (sh->focus)
	{
		sh->focus = tmp;
		tmp = tmp->prev;
		while (tmp != sh->begin_line && ft_isspace(tmp->input))
			tmp = tmp->prev;
		if (tmp && tmp != sh->begin_line)
			sh->focus = ft_sh_backword_on_word(tmp, sh);
	}
	else
		sh->focus = sh->end;
	ft_move_cursor_backword(sh, y);
	return (1);
}
