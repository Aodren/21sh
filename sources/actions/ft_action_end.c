#include "ft_shell.h"
#include "libft.h"

int		ft_sh_end(t_21sh *sh)
{
	unsigned int		cursor_y;

	if (!sh->focus)
		return (0);
	ft_calcul_pos_cursor(sh);
	cursor_y = sh->cursor_y;
	sh->focus = 0;
	ft_calcul_pos_cursor(sh);
	while (cursor_y < sh->cursor_y)
	{
		ft_termcaps_cursor_down();
		cursor_y++;
	}
	ft_calcul_pos_cursor(sh);
	ft_termcaps_cursor_nright(sh->cursor_x);
	return (1);
}
