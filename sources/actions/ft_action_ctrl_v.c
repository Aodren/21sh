#include "ft_shell.h"
#include "libft.h"

int		ft_sh_ctrl_v(t_21sh *sh)
{
	if (IS_CTRL_V(sh->parse))
	{
		ft_add_sentence('^', sh, 0);
		ft_add_sentence('V', sh, 0);
	}
	CTRL_V(sh->parse);
	return (1);
}
