#include "ft_shell.h"
#include "libft.h"

int		ft_sh_echap(t_21sh *sh)
{
	t_sent *tmp;

	if (sh->insertion)
		sh->insertion = 0;
	else
		return (0);
	tmp = sh->begin;
	while (tmp)
	{
		if (tmp->highlight)
			tmp->highlight = 0;
		tmp = tmp->next;
	}
	if (sh->focus_insertion)
	{
		sh->focus_insertion->highlight = 0;
		sh->focus_insertion = 0;
	}
	sh->last_action = 2;
	ft_calcul_pos_cursor(sh);
	ft_display_reput_line(sh, ECHAP);
	return (1);
}
