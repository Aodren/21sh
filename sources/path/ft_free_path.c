#include <stdlib.h>
#include "ft_shell.h"

void	ft_free_path(t_path *path)
{
	if (path)
	{
		if (path->next)
			ft_free_path(path->next);
		if (path->path)
			free(path->path);
		free(path);
	}
}
