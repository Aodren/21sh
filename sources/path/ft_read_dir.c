#include <dirent.h>
#include "libft.h"
#include "ft_shell.h"
#include "ft_trie.h"

void	ft_read_dir_trie(t_21sh *sh, char *dir)
{
	DIR				*dirp;
	struct dirent	*dp;

	if (!(dirp = opendir(dir)))
		return ;
	while ((dp = readdir(dirp)))
	{
		if (ft_strcmp(dp->d_name, ".") && ft_strcmp(dp->d_name, ".."))
			ft_create_trie(sh, dp, dir);
	}
	closedir(dirp);
}
