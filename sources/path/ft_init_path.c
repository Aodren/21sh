#include "libft.h"
#include "ft_shell.h"

static t_path	*ft_init_elem_path(t_path *new, char *path, size_t size)
{
	if (!(new = ft_memalloc(sizeof(t_path))))
		ft_display_error(MALLOC);
	new->path = ft_strndup(path, size);
	new->next = NULL;
	return (new);
}

void			ft_add_path(t_21sh *sh, char *path, size_t size)
{
	t_path *new;

	new = NULL;
	new = ft_init_elem_path(new, path, size);
	ft_read_dir_trie(sh, new->path);
	if (!sh->begin_path)
	{
		sh->begin_path = new;
		sh->end_path = new;
	}
	else
	{
		sh->end_path->next = new;
		sh->end_path = new;
	}
}

void			ft_init_path(t_21sh *sh)
{
	sh->path = ft_getenv("PATH", sh->env);
	ft_recup_path(sh);
}
