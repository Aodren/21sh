#include "libft.h"
#include "ft_shell.h"

void	ft_recup_path(t_21sh *sh)
{
	char	*path;
	char	*d;

	path = sh->path;
	d = path;
	while (*path)
	{
		if (*path == ':')
		{
			ft_add_path(sh, d, path - d);
			d = path + 1;
		}
		++path;
	}
	ft_add_path(sh, d, path - d);
}
