#include "libft.h"
#include "ft_shell.h"

/*
 ** Renvoi un pointeur sur la valeur de la variable
 */



static char	*ft_search_v(char **i, char *variable, size_t s)
{
	while (*i)
	{
		if (!ft_strncmp(variable, *i, s))
		{
			if (*(*i + s) == '=')
				return (*i + s + 1);
		}
		++i;
	}
	return (0);
}

char	*ft_get_value_variable(t_21sh *sh, char *variable)
{
	char	*value;
	size_t	s;

	s = ft_strlen(variable);
	if ((value = ft_search_v(sh->env, variable, s)))
		return (value);
	if ((value = ft_search_v(sh->v_locales, variable, s)))
		return (value);
	return (0);
}
