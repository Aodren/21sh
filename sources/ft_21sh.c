#include "ft_shell.h"
#include "libft.h"

void	ft_21sh(t_21sh *sh)
{
	int ret;

	ft_putstr(sh->prompt);
	ft_termcaps_save_position();
	while (1)
	{
		if ((ret = ft_read_input(sh)) >= 0)
			ret = sh->actions[ret](sh);
	}
}
