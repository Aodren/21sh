#include "ft_shell.h"
#include "libft.h"



#include "ft_lexer.h"
int		main(int argc, char **argv, char **env)
{
	t_21sh *sh;

	(void)argc;
	(void)argv;
	sh = (void *)0;
	sh = ft_init_shell(sh);
	ft_create_env(sh, env);
	ft_termcaps_clear_tab_stop(); // a clear


	// pour les tests
	sh->v_locales = ft_strsplit("tata=Yes\ntoto=caca\ntiti=lala\n", '\n');

	char **test = sh->v_locales;
	while (*test)
		ft_putendl(*test++);

	char	*t;

	(void)t;
	t = ft_get_value_variable(sh, "TERM");


	ft_21sh(sh);
	ft_destruct_shell(sh);
	(void)argc;
	(void)argv;
	(void)env;
	return (0);
}
