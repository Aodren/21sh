#include "ft_lexer.h"
#include "ft_shell.h"
#include "libft.h"

int		ft_lexer_single_quotes(t_token *token, t_lexer *lexer,t_21sh *sh)
{
	t_sent 			*tmp;
	unsigned int 	size;

	tmp = lexer->focus;
	size = 0;
	(void)sh;
	while (tmp)
	{
		if (tmp->input == '\\')
		{
			if (!tmp->next)
				return (0);
			if (tmp->next->input == '\'' && !tmp->next->next)
				return (0);
		}
		if (tmp->input == '\'')
		{
			ft_malloc_value(lexer, token, size);
			lexer->focus = tmp->next;
			return (1);
		}
		tmp = tmp->next;
		++size;
	}
	return (0);
}
