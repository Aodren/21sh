#include "ft_lexer.h"
#include "ft_shell.h"


int			ft_lexer_strdupequ(t_token *token, t_lexer *lexer, int c)
{
	t_sent	*tmp;
	int size;

	tmp = lexer->focus;
	size = 0;
	while (tmp && tmp->input != (unsigned int)c)
	{
		tmp = tmp->next;
		++size;
	}
	if (!tmp)
		return (0);
	ft_malloc_value(lexer, token, size);
	lexer->focus = tmp;
	return (1);
}
