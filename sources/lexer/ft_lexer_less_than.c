#include "ft_shell.h"
#include "ft_lexer.h"
#include "libft.h"

// par defut on lui envoi lentre zero
int		ft_lexer_less_than(t_21sh *sh, t_token *token, t_lexer *lexer)
{
	t_sent	*search;
	unsigned int size;

	(void)sh;
	search = lexer->focus;
	size = 1;
	while (search && ft_isdigit(search->input))
	{
		search = search->next;
		++size;
	}
	if (!search)
		return (0);
	if (search->input == '<')
	{
		ft_malloc_value(lexer, token , size);
		lexer->focus = search->next;
		token->token = LESS_THAN;
		lexer->f_tokens[WORD] = ft_lexer_files;
		return (1);
	}
	return (0);
}
