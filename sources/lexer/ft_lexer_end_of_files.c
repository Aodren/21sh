#include "ft_shell.h"
#include "ft_lexer.h"

int		ft_lexer_end_of_files(t_21sh *sh, t_token *token, t_lexer *lexer)
{
	t_sent 			*search;
	unsigned int 	size;

	size = 0;
	search = ft_lstpbrk(lexer->focus, " \\;&|\"'`()><", &size);
	if (search && (ft_is_echapement(search->input)))
	{
		token->token = END_OF_FILE;
		if (search->input == '\\')
		{
			if (!(search->next))
			{
				ft_malloc_value(lexer, token, size);
				lexer->focus = search->next;
				return (2);
			}
			ft_malloc_value(lexer, token, size + 1);
			lexer->focus = search->next->next;
			token->value[token->size - 1] = search->next->input;
		}
		else
		{
			ft_malloc_value(lexer, token, size);
			lexer->focus = search->next;
			if (search->input == '"')
			{
				if (!ft_lexer_strdupequ(token, lexer,'"'))
				{
					lexer->focus = search->next;
					return (2);
				}
			}
			else if (search->input == '\'')
				if (!ft_lexer_strdupequ(token, lexer, '\''))
				{
					lexer->focus = search->next;
					return (2);
				}
		}
		if (lexer->cmd)
			lexer->f_tokens[WORD] = ft_lexer_arg;
		else
			lexer->f_tokens[WORD] = ft_lexer_cmd;
		return (1 + ft_lexer_end_of_files(sh, token, lexer));
	}
	else if (search != lexer->focus)
	{
		token->token = token->token == 0 ? END_OF_FILE : token->token;
		ft_malloc_value(lexer, token, size);
		lexer->focus = search;
		if (lexer->cmd)
			lexer->f_tokens[WORD] = ft_lexer_arg;
		else
			lexer->f_tokens[WORD] = ft_lexer_cmd;
		return (1);
	}
	return (0);
}
