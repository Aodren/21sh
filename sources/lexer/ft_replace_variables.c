#include "ft_lexer.h"
#include "ft_shell.h"
#include "libft.h"
#include <stdlib.h>

static char	*ft_create_name_variable(t_sent *focus, t_sent *search, unsigned int 
		size)
{
	char *name;
	char *d;

	name = ft_memalloc(sizeof(char) * (size + 1));
	d = name;
	while (focus && focus != search && size)
	{
		*name++ = focus->input;
		focus = focus->next;
		--size;
	}
	return (d);
}

void	ft_lexer_variable(t_token *token, t_lexer *lexer, t_21sh *sh)
{
	t_sent 		*search;
	unsigned int size;
	char		*name_variable;
	char		*value;

	size = 0;
	search = ft_lstpbrk(lexer->focus, " \\;=&|\"'`{()}><$", &size);

	name_variable = ft_create_name_variable(lexer->focus, search, size);
//	ft_printf("nom de la variable %s\n", name_variable);
	value = ft_get_value_variable(sh, name_variable);
	free(name_variable);
	name_variable = 0;
	if ((name_variable = token->value))
	{
		if (value)
		{
			token->value = ft_strjoin(name_variable, value);
			ft_putendl(token->value);
			token->size += ft_strlen(value);
			free(name_variable);
		}
	}
	else
	{
		token->value = ft_strdup(name_variable);
		token->size += ft_strlen(token->value);
	}
//	lexer->focus = search == 0 ? search : search->next;
	lexer->focus = search;
	/*
	if (lexer->focus)
		ft_printf("valeur du focus a la fin de varaible : %c\n", lexer->focus->input);
		*/
	return ;
}
