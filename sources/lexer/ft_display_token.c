#include "libft.h"
#include "ft_lexer.h"

void		ft_display_token(t_token *tmp)
{
	ft_putendl("**************************");
	while (tmp)
	{
		if (tmp->token == CMD)
				ft_putendl("TOKEN CMD :");
		else if (tmp->token == NVARIABLE)
			ft_putendl("TOKEN NOM VARIABLE");
		else if (tmp->token == O_BRACKETS)
			ft_putendl(" TOKEN O_BRACKETS");
		else if (tmp->token == C_BRACKETS)
			ft_putendl(" TOKEN C_BRACKETS");
		else if (tmp->token == END_OF_STATEMENT)
			ft_putendl("END OF STATEMENT");
		else if (tmp->token == ARG)
			ft_putendl("TOKEN ARG");
		else if (tmp->token == EQUAL)
			ft_putendl("token equal");
		else if (tmp->token == VALUE)
			ft_putendl("TOKEN VALUE");
		else if (tmp->token == COMMENT)
			ft_putendl("TOKEN COMMENT");
		else if (tmp->token == T_BACKSLASH)
			ft_putendl("TOKEN T_BACKSLAH");
		else if (tmp->token == AND)
			ft_putendl("TOKEN AND");
		else if (tmp->token == BACKGROUND)
			ft_putendl("TOKEN BACKGROUND");
		else if (tmp->token == LESS_THAN)
			ft_putendl("TOKEN LESS_THAN");
		else if (tmp->token == MORE_THAN)
			ft_putendl("TOKEN MORE_THAN");
		else if (tmp->token == PIPE)
			ft_putendl("Token PIPE");
		else if (tmp->token == OR)
			ft_putendl("Token OR");
		else if (tmp->token == D_MORE_THAN)
			ft_putendl("Token D_MORE_THAN");
		else if (tmp->token == D_LESS_THAN)
			ft_putendl("Token D_lESS_THAN");
		else if (tmp->token == T_FILE)
			ft_putendl("TOKEN  T_FILE");
		else if (tmp->token == END_OF_FILE)
			ft_putendl("TOKEN  END_OF_FILE");
		else if (tmp->token == AMBIGOUS)
			ft_putendl("Token AMBIHOUS");
		else if (tmp->token == AGREGATION)
			ft_putendl("AGREGATION de fichier");
		else if (tmp->token == WHITESPACES)
			ft_putendl("TOKEN WHITESPACES");
		else if (tmp->token == MQUOTES)
			ft_putendl("TOKEN MQUOTES");
		else
		{
			ft_putnbr(tmp->token);
			ft_putendl("TOKEN INCONNU");
		}
		ft_putstr("value : ");
		if (tmp->value)
			ft_putendl(tmp->value);
		else
			ft_putendl("(null)");
		tmp = tmp->next;
	}
	ft_putendl("**************************");

}
