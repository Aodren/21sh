#include "ft_shell.h"
#include "ft_lexer.h"
#include "libft.h"

// par defut c'ets unn 
int		ft_lexer_more_than(t_21sh *sh, t_token *token, t_lexer *lexer)
{
	t_sent	*search;
	unsigned int size;

	search = lexer->focus;
	size = 1;
	(void)sh;
	while (search && ft_isdigit(search->input))
	{
		search = search->next;
		++size;
	}
	if (!search)
		return (0);
	if (search == lexer->focus && search->input == '&')
	{
		search = search->next;
		++size;
	}
	if (search->input == '>') 
	{
		ft_malloc_value(lexer, token , size);
		lexer->focus = search->next;
		token->token = MORE_THAN;
		lexer->f_tokens[WORD] = ft_lexer_files;
		return (1);
	}
	return (0);
}
