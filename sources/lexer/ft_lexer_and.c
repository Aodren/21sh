#include "ft_shell.h"
#include "ft_lexer.h"
#include "libft.h"

int		ft_lexer_and(t_21sh *sh, t_token *token, t_lexer *lexer)
{
	(void)sh;
	if (lexer->focus->input == '&' && lexer->focus->next && 
			lexer->focus->next->input == '&')
	{
		lexer->f_tokens[WORD] = ft_lexer_cmd;
		lexer->focus = lexer->focus->next->next;
		token->token = AND;
		lexer->cmd = 0;
		return (1);
	}
	return (0);
}
