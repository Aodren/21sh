#include "libft.h"
#include "ft_lexer.h"

t_lexer		*ft_init_lexer(t_lexer *lexer)
{
	lexer = ft_memalloc(sizeof(t_lexer));
	lexer->begin = 0;
	lexer->state = 1;
	lexer->f_tokens[MORE_THAN] = ft_lexer_more_than;
	lexer->f_tokens[LESS_THAN] = ft_lexer_less_than;
	lexer->f_tokens[AGREGATION] = ft_lexer_agregation;
	lexer->f_tokens[WHITESPACES] = ft_lexer_whitespaces;
	lexer->f_tokens[T_BACKSLASH] = ft_lexer_backslash;
	lexer->f_tokens[COMMENT] = ft_lexer_comment;
	lexer->f_tokens[AND] = ft_lexer_and;
	lexer->f_tokens[BACKGROUND] = ft_lexer_background;
	lexer->f_tokens[PIPE] = ft_lexer_pipe;
	lexer->f_tokens[OR] = ft_lexer_or;
	lexer->f_tokens[POPEN] = ft_lexer_popen;
	lexer->f_tokens[PCLOSE] = ft_lexer_pclose;
	lexer->f_tokens[MQUOTES] = ft_lexer_mquotes;
	lexer->f_tokens[END_OF_STATEMENT] = ft_lexer_end;
	lexer->f_tokens[EQUAL] = ft_lexer_equal;
	lexer->f_tokens[O_BRACKETS] = ft_lexer_open_brackets;
	lexer->f_tokens[C_BRACKETS] = ft_lexer_close_brackets;
	lexer->f_tokens[D_LESS_THAN] = ft_lexer_d_less_than;
	lexer->f_tokens[D_MORE_THAN] = ft_lexer_d_more_than;
	return (lexer);
}

t_token		*ft_init_token(t_token *token, t_lexer *lexer)
{
	token = ft_memalloc(sizeof(t_token));
	token->value = 0;
	if (!lexer->begin)
	{
		lexer->begin = token;
		lexer->end = token;
	}
	else
	{
		token->prev = lexer->end;
		lexer->end->next = token;
		lexer->end = token;
	}
	return (token);
}
