#include "libft.h"
#include "ft_lexer.h"
#include <stdlib.h>


static void	ft_lstmove(t_sent *focus, char *str, unsigned int size)
{
	while (size && focus)
	{
		*str++ = focus->input;
		focus = focus->next;
		--size;
	}
}

void		ft_malloc_value(t_lexer *lexer, t_token *token, unsigned int size)
{
	char	*str;

	if (!token->value)
	{
		token->value = ft_memalloc(sizeof(char) * (size + 1));				
		ft_lstmove(lexer->focus, token->value, size);
	}
	else
	{
		str = ft_memalloc(sizeof(char) * (token->size + size + 1));
		ft_memmove(str, token->value, token->size);
		ft_lstmove(lexer->focus, str + token->size, size);
		free(token->value);
		token->value = str;
	}
	token->size += size;
}
