#include "ft_shell.h"
#include "ft_lexer.h"
#include "libft.h"

int		ft_lexer_pipe(t_21sh *sh, t_token *token, t_lexer *lexer)
{
	(void)sh;
	if (lexer->focus->input == '|')
	{
		token->token = PIPE;
		lexer->focus = lexer->focus->next;
		lexer->f_tokens[WORD] = ft_lexer_cmd;
		return (1);
	}
	return (0);
}
