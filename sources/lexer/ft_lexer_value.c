#include "ft_shell.h"
#include "ft_lexer.h"
#include "libft.h"

int		ft_lexer_value(t_21sh *sh, t_token *token, t_lexer *lexer)
{
	t_sent 			*search;
	unsigned int 	size;

	size = 0;
	search = ft_lstpbrk(lexer->focus, " \\;&|\"'`()><$", &size);
	if (search && ft_is_echapement(search->input))
	{
		token->token = VALUE;
		if (search->input == '\\')
		{
			ft_malloc_value(lexer, token, size + 1);
			if (!(search->next))
			{
				lexer->focus = search;
				return (3);
			}
			lexer->focus = search->next->next;
			token->value[token->size - 1] = search->next->input;
		}
		else
		{
			ft_malloc_value(lexer, token, size);
			lexer->focus = search->next;
			if (search->input == '$')
				ft_lexer_variable(token, lexer, sh);
			else if (search->input == '"')
				if (!ft_lexer_double_quotes(token, lexer, sh))
				{
					lexer->focus = search;
					return (2);
				}
		}
		return (1 + ft_lexer_value(sh, token, lexer));
	}
	else if (search != lexer->focus)
	{
		token->token = VALUE;
		ft_malloc_value(lexer, token, size);
		lexer->focus = search;
	}
	return (0);
}
