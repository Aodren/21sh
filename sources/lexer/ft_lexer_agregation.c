#include "ft_shell.h"
#include "ft_lexer.h"
#include "libft.h"

int		ft_lexer_agregation(t_21sh *sh, t_token *token, t_lexer *lexer)
{
	t_sent 			*search;
	t_sent			*tmp;
	unsigned int 	size;
	unsigned int	nbr;

	search = lexer->focus;
	size = 1;
	(void)sh;
	while (search && ft_isdigit(search->input))
	{
		search = search->next;
		++size;
	}
	if (!search)
		return (0);
	if (search->input == '>' && search->next && search->next->input == '&')
	{
		search = search->next->next;
		tmp = search;
		size += 1;
		nbr = 0;
		while (search && ft_isdigit(search->input))
		{
			search = search->next;
			nbr++;
		}
		if (search && ft_isalpha(search->input))
		{
			ft_malloc_value(lexer, token, size);
			lexer->focus = tmp;
		}
		else
		{
			if (search && search->input == '-')
			{
					++nbr;
					search = search->next;
			}
			ft_malloc_value(lexer, token, size + nbr);
			lexer->focus = search ? search->next : (void *)0;
		}
		token->token = AGREGATION;
		if (lexer->cmd)
			lexer->f_tokens[WORD] = ft_lexer_arg;
		else
			lexer->f_tokens[WORD] = ft_lexer_cmd;
		return (1);
	}
	return (0);
}
