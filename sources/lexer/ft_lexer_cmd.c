#include "ft_shell.h"
#include "ft_lexer.h"
#include "libft.h"

int		ft_lexer_cmd(t_21sh *sh, t_token *token, t_lexer *lexer)
{
	t_sent 			*search;
	unsigned int 	size;

	size = 0;
	search = ft_lstpbrk(lexer->focus, " \\;=&|\"'`()><${", &size);
	if (search && (ft_is_echapement(search->input) || search->input 
				== '{'))
	{
		token->token = CMD;
		lexer->cmd = 1;
		if (search->input == '\\')
		{
			if (!(search->next))
			{
				ft_malloc_value(lexer, token, size);
				lexer->focus = search->next;
				return (2);
			}
			ft_malloc_value(lexer, token, size + 1);
			lexer->focus = search->next->next;
			token->value[token->size - 1] = search->next->input;
		}
		else if (search->input == '{')
		{
			ft_malloc_value(lexer, token, size + 1);
			token->value[token->size - 1] = '{';
			lexer->focus = search->next;
		}
		else
		{
			ft_malloc_value(lexer, token, size);
			lexer->focus = search->next;
			if (search->input == '$')
				ft_lexer_variable(token, lexer, sh);
			else if (search->input == '"')
			{
				if (!ft_lexer_double_quotes(token, lexer, sh))
				{
					lexer->focus = search->next;
					return (2);
				}
			}
			else if (search->input == '\'')
			if (!ft_lexer_single_quotes(token, lexer, sh))
				{
					lexer->focus = search->next;
					return (2);
				}
		}
		lexer->f_tokens[WORD] = ft_lexer_arg;
		return (1 + ft_lexer_cmd(sh, token, lexer));
	}
	else if (search && search->input == '=')
	{
		lexer->cmd = 1;
		if (token->token == CMD || search == lexer->focus)
		{
			ft_malloc_value(lexer, token, size + 1);
			lexer->focus = search->next;
			return (1 + ft_lexer_cmd(sh, token, lexer));
		}
		else
		{
			token->token = NVARIABLE;
			lexer->f_tokens[WORD] = ft_lexer_value;
			ft_malloc_value(lexer, token, size);
			lexer->focus = search;
			return (1);
		}
	}
	else if (search != lexer->focus)
	{
		lexer->cmd = 1;
		token->token = token->token == 0 ? CMD : token->token;
		ft_malloc_value(lexer, token, size);
		lexer->focus = search;
		lexer->f_tokens[WORD] = ft_lexer_arg;
		return (1);
	}
	return (0);
}
