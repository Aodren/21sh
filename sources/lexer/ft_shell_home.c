#include <sys/stat.h>
#include "ft_shell.h"

#include "libft.h"
#include <dirent.h>
void	ft_shell_home(t_token *token, t_21sh *sh, t_lexer *lexer)
{
	char	*home;
	(void)token;
	(void)sh;
	(void)lexer;


	if (lexer->focus && lexer->focus->input == '~')
	{
		if ((home = ft_getenv("HOME", sh->env)))
		{
			token->value = ft_strdup(home);
			token->size = ft_strlen(home);
			lexer->focus = lexer->focus->next;
//			token->token = CMD;
		}
	}
}
