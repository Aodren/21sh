#include "ft_shell.h"
#include "ft_lexer.h"
#include "libft.h"

int		ft_lexer_backslash(t_21sh *sh, t_token *token, t_lexer *lexer)
{
	(void)sh;
	if (lexer->focus->input == '\\' &&  !lexer->focus->next)
	{
		token->token = T_BACKSLASH;
		lexer->focus = lexer->focus->next;
		token->token = T_BACKSLASH;
		if (!lexer->focus)
			return  (2);
		return (1);
	}

	return (0);
}
