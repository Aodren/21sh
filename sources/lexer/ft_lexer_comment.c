#include "ft_shell.h"
#include "ft_lexer.h"
#include "libft.h"

int		ft_lexer_comment(t_21sh *sh, t_token *token, t_lexer *lexer)
{

	(void)sh;
	if (lexer->focus->input == '#' && (!lexer->end->prev || lexer->end->prev->token
		   	!= EQUAL))
	{
		token->token = COMMENT;
		lexer->focus = 0;
		return (1);
	}
	return (0);
}
