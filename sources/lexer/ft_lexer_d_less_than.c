#include "ft_lexer.h"
#include "ft_shell.h"
#include "libft.h"

// par defaut fd a zero
int		ft_lexer_d_less_than(t_21sh *sh, t_token *token, t_lexer *lexer)
{
	t_sent	*search;
	unsigned int size;

	search = lexer->focus;
	size = 1;
	(void)sh;
	while (search && ft_isdigit(search->input))
	{
		search = search->next;
		++size;
	}
	if (!search)
		return (0);
	if (search->input == '<' && search->next && 
			search->next->input == '<')
	{
		ft_malloc_value(lexer, token , size);
		lexer->focus = search->next->next;
		token->token = D_LESS_THAN;
		lexer->f_tokens[WORD] = ft_lexer_end_of_files;
		return (1);
	}
	return (0);
}
