#include "ft_shell.h"
#include "ft_lexer.h"
#include "libft.h"

int		ft_lexer_mquotes(t_21sh *sh, t_token *token, t_lexer *lexer)
{
	(void)sh;
	(void)token;
	(void)lexer;

	t_sent *search;
	unsigned int size;

	if (lexer->focus->input == '`')
	{
		search = ft_lstpbrk(lexer->focus->next, "\\`", &size);
		if (search != lexer->focus)
		{
			token->token = MQUOTES;
			if (!search)
			{
				lexer->focus = 0;
				return (2);
			}
			if (search->input == '\\')
			{
					ft_putendl("lolo");
				if (!search->next || !search->next->next)
				{
					ft_malloc_value(lexer, token, size);
					lexer->focus = search->next;
					return (2);

				}
				ft_malloc_value(lexer, token , size);
				lexer->focus = search->next->next + 1;
				token->value[token->size - 1] = search->next->input;
				return (1 + ft_lexer_mquotes(sh, token, lexer));
			}
			ft_putendl("lolololololo");
			ft_malloc_value(lexer, token , ++size);
			lexer->focus = search->next;
			return (1);
		}
	}
	return (0);
}
