#include "ft_shell.h"
#include "ft_lexer.h"
#include "libft.h"

int		ft_lexer_whitespaces(t_21sh *sh, t_token *token, t_lexer *lexer)
{
	int ok;

	ok = 0;
	(void)token;
	(void)sh;
	while (lexer->focus && ft_isspace(lexer->focus->input))
	{
		ok = 1;
		lexer->focus = lexer->focus->next;
	}
	if (ok)
	{
		if (lexer->end->prev && lexer->end->prev->token == EQUAL)
			lexer->f_tokens[WORD] = ft_lexer_cmd;
		if (!lexer->focus)
			token->token = WHITESPACES;
		return (1);
	}
	else
		return (0);
}
