#include "ft_shell.h"
#include "ft_lexer.h"
#include "libft.h"

int		ft_lexer_popen(t_21sh *sh, t_token *token, t_lexer *lexer)
{
	(void)sh;
	if (lexer->focus->input == '(')
	{
		lexer->focus = lexer->focus->next;
		token->token = POPEN;
		return (1);
	}
	return (0);
}
