#include "ft_shell.h"
#include "ft_lexer.h"
#include "libft.h"

int		ft_lexer_open_brackets(t_21sh *sh, t_token *token, t_lexer *lexer)
{
	(void)sh;
	if (lexer->focus->input == '{' && (!lexer->focus->next ||
				ft_isspace(lexer->focus->next->input)
				))
	{
		lexer->focus = lexer->focus->next;
		token->token = O_BRACKETS;
//		lexer->open_brackets = 0;
//		lexer->f_tokens[WORD] = ft_lexer_cmd;
		return (1);
	}
	return (0);
}
