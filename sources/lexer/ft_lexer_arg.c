#include "ft_shell.h"
#include "ft_lexer.h"
#include "libft.h"

int		ft_lexer_arg(t_21sh *sh, t_token *token, t_lexer *lexer)
{
	t_sent 			*search;
	unsigned int 	size;

	size = 0;
	search = ft_lstpbrk(lexer->focus, " \\;&|\"'`()><$", &size);
	if (search && ft_is_echapement(search->input))
	{
		token->token = ARG;
		if (search->input == '\\')
		{
			if (!(search->next))
			{
				ft_malloc_value(lexer, token, size);
				lexer->focus = search->next;
				return (2);
			}
			ft_malloc_value(lexer, token, size + 1);
			lexer->focus = search->next->next;
			token->value[token->size - 1] = search->next->input;
		}
		else
		{
			ft_malloc_value(lexer, token, size);
			lexer->focus = search->next;
			if (search->input == '$')
				ft_lexer_variable(token, lexer, sh);
			else if (search->input == '"')
			{
				if (!ft_lexer_double_quotes(token, lexer, sh))
				{
					lexer->focus = search;
					return (2);
				}
			}
			else if (search->input == '\'')
			if (!ft_lexer_single_quotes(token, lexer, sh))
				{
					lexer->focus = search->next;
					return (2);
				}
		}
		return (1 + ft_lexer_arg(sh, token, lexer));
	}
	if (search != lexer->focus)
	{
		token->token = token->token == 0 ? ARG : token->token;
		ft_malloc_value(lexer, token, size);
		lexer->focus = search;
		return (1);
	}
	return (0);
}
