#include "ft_shell.h"
#include "ft_lexer.h"
#include "libft.h"

int		ft_lexer_files(t_21sh *sh, t_token *token, t_lexer *lexer)
{
	t_sent 			*search;
	unsigned int 	size;

	size = 0;
	search = ft_lstpbrk(lexer->focus, " \\;&|\"'`()><$", &size);
	if (search && (ft_is_echapement(search->input)))
	{
		token->token = T_FILE;
		if (lexer->cmd)
			lexer->f_tokens[WORD] = ft_lexer_arg;
		else
			lexer->f_tokens[WORD] = ft_lexer_cmd;
		if (search->input == '\\')
		{
			if (!(search->next))
			{
				ft_malloc_value(lexer, token, size);
				lexer->focus = search->next;
				return (2);
			}
			ft_malloc_value(lexer, token, size + 1);
			lexer->focus = search->next->next;
			token->value[token->size - 1] = search->next->input;
		}
		else
		{
			ft_malloc_value(lexer, token, size);
			lexer->focus = search->next;
			if (search->input == '$')
			{
				search = lexer->focus;
				ft_lexer_variable(token, lexer, sh);
				if (!*token->value)
				{
					lexer->focus = search->prev;
					search = ft_lstpbrk(lexer->focus, " \\;=&|\"'`{()}><", &size);
					ft_malloc_value(lexer, token, size);
					if (search)
						lexer->focus = search->next;
					else
						lexer->focus = 0;
					token->token = AMBIGOUS;
					return (1);
				}
			}
			else if (search->input == '"')
			{
				if (!ft_lexer_double_quotes(token, lexer, sh))
				{
					lexer->focus = search->next;
					return (2);
				}
			}
			else if (search->input == '\'')
			if (!ft_lexer_single_quotes(token, lexer, sh))
				{
					lexer->focus = search->next;
					return (2);
				}
	
		}
		return ( 1 + ft_lexer_files(sh, token, lexer));
	}
	else if (search != lexer->focus)
	{
		token->token = token->token == 0 ? T_FILE : token->token;
		ft_malloc_value(lexer, token, size);
		lexer->focus = search;
		if (lexer->cmd)
		{
			lexer->f_tokens[WORD] = ft_lexer_arg;
		}
		else
		{
			lexer->f_tokens[WORD] = ft_lexer_cmd;
		}
		
		return (1);
	}
	return (0);
}
