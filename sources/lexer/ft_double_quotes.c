#include "ft_lexer.h"
#include "ft_shell.h"
#include "libft.h"

int		ft_lexer_double_quotes(t_token *token, t_lexer *lexer,t_21sh *sh)
{
	t_sent 			*tmp;
	unsigned int 	size;

	tmp = lexer->focus;
	size = 0;
	while (tmp)
	{
		if (tmp->input == '\\')
		{
			if (!tmp->next)
				return (0);
			ft_malloc_value(lexer, token, size + 1);
			lexer->focus = tmp->next->next;
			token->value[token->size - 1] = tmp->next->input;
			if (ft_lexer_double_quotes(token, lexer, sh))
				return (1);
			else
				return (0);
		}
		if (tmp->input == '"')
		{
			ft_malloc_value(lexer, token, size);
			lexer->focus = tmp->next;
			return (1);
		}
		if (tmp->input == '$')
		{
			ft_malloc_value(lexer, token, size);
			lexer->focus = tmp->next;
			ft_lexer_variable(token, lexer, sh);
			if (ft_lexer_double_quotes(token, lexer, sh))
				return (1);
			return (0);
		}	
		tmp = tmp->next;
		++size;
	}
	return (0);
}
