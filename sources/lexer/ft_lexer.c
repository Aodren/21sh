#include "ft_shell.h"
#include "ft_lexer.h"
#include "libft.h"

int		ft_lexer(t_21sh *sh)
{
	unsigned int nbr;
	unsigned int ret;
	t_lexer		*lexer;
	t_token		*token;

	lexer = sh->lexer;
	lexer->focus = sh->begin;
	ft_putchar('\n');
	lexer->f_tokens[WORD] = ft_lexer_cmd;
	lexer->cmd = 0;
	lexer->open_brackets = 0;
	token = 0;
	ret = 0;
	while (lexer->focus)
	{
		nbr = 1;

		if (!token || token->token)
			token = ft_init_token(token, lexer);
		ft_shell_home(token, sh, lexer);
		while (nbr < NBR_TOKEN)
		{
			ret = lexer->f_tokens[nbr](sh, token, lexer);
			if (ret == 2)
			{
				ft_free_tokens(lexer->begin);
				lexer->focus = 0;
				return (MULTI_LINES);
			}
			if (ret || !lexer->focus)
				break ;
			++nbr;

		}
	}
	// c'est le lexer aui afficheras lexer error
	if (!lexer->begin)
		return (EMPTY_LINE);
	ft_display_token(lexer->begin);
	ft_free_tokens(lexer->begin);
	lexer->begin = 0;
	lexer->state = CMD;
	return (SINGLE_LINE);
}
