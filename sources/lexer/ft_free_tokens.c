#include "ft_lexer.h"
#include <stdlib.h>

#include "libft.h"
void	ft_free_tokens(t_token *begin)
{
	if (begin)
	{
		ft_free_tokens(begin->next);
		if (begin->value)
			free(begin->value);
		free(begin);
		begin = 0;
	}
}	
