#include "ft_shell.h"
#include "ft_lexer.h"
#include "libft.h"

int		ft_lexer_equal(t_21sh *sh, t_token *token, t_lexer *lexer)
{
	if (lexer->focus->input == '=' && lexer->end->prev && lexer->end->prev->
			token != CMD)
	{
		lexer->focus = lexer->focus->next;
		token->token = EQUAL;
		return (1);
	}
	else if (lexer->focus->input == '=')
	{
		if (lexer->end->prev && lexer->end->prev->token == CMD)
		{
			token->token = ARG;
			ft_lexer_arg(sh, token, lexer);
		}
		else
		{
			token->token = CMD;
			ft_lexer_cmd(sh, token, lexer);
		}
		return (1);
	}
	return (0);
}
