#include "ft_shell.h"
#include "ft_lexer.h"
#include "libft.h"

int		ft_lexer_end(t_21sh *sh, t_token *token, t_lexer *lexer)
{
	(void)sh;
	if (lexer->focus->input == ';')
	{
		lexer->focus = lexer->focus->next;
		token->token = END_OF_STATEMENT;
		lexer->open_brackets = 0;
		lexer->cmd = 0;
		lexer->f_tokens[WORD] = ft_lexer_cmd;
		return (1);
	}
	return (0);
}
