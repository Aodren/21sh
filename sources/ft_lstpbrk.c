#include "ft_shell.h"

/*
** Renvoie le pointeur sur un des elements contenues dans str2,,
** *size -> increment de la taille
*/

t_sent	*ft_lstpbrk(t_sent *begin, const char *str2, unsigned int *size)
{
	char	*d;

	while (begin)
	{
		d = (char *)str2;
		while (*d)
		{
			if ((unsigned char)*d == begin->input)
				return (begin);
			++d;
		}
		++*size;
		begin = begin->next;
	}
	return (begin);
}
