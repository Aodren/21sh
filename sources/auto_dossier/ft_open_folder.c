#include "libft.h"
#include <dirent.h>
#include "ft_shell.h"

void	ft_handle_folder_no_name(t_21sh *sh, unsigned int *size_max,
		unsigned int *nbr)
{
	DIR				*dirp;
	struct dirent	*dp;

	if ((dirp = opendir(".")))
	{
		while ((dp = readdir(dirp)))
		{
			if (*(dp->d_name) != '.')
			{
				ft_add_folder_info(sh, dp->d_name, size_max);
				*nbr += 1;
			}
		}
	}
	if (dirp)
		closedir(dirp);
}

void	ft_handle_something(char *folder, char *name, t_21sh *sh,
		unsigned int *tab)
{
	DIR				*dirp;
	struct dirent	*dp;

	if ((dirp = opendir(folder)))
	{
		while ((dp = readdir(dirp)))
		{
			if (!name || ft_strchrstart(dp->d_name, name))
			{
				if (name || (!name && *(dp->d_name) != '.'))
				{
					ft_add_folder_info(sh, dp->d_name, &tab[1]);
					tab[0] += 1;
				}
			}
		}
	}
	if (dirp)
		closedir(dirp);
}
