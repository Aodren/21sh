#include "ft_shell.h"
#include "ft_struct.h"
#include "ft_trie.h"
#include "libft.h"
#include <dirent.h>
#include <stdlib.h>
#include <dirent.h>

static char	*ft_close_folder(char *folder, DIR *dp)
{

	closedir(dp);
	return (folder);
}

static char	*ft_skip_check(char *name, size_t *size)
{
	while (*name)
	{
		++name;
		++*size;
	}
	return (name);
}

char	*ft_check_if_folder(char *name, char **new_name)
{
	char		*folder;
	size_t		size;
	DIR			*dp;
	char		*d;

	folder = 0;
	d = name;
	if (((*name == '.' && *(name + 1) == 0) ||
				(*name == '.' && *(name + 1) == '.' && !*(name + 2))))
		return (folder);
	size = 0;
	name = ft_skip_check(name, &size);
	while (size && *(d + (size) - 1) != '/')
		--size;
	if (size)
	{
		folder = ft_strndup(d, sizeof(char) * (size));
		if (name - d - size)
			*new_name = ft_strndup(d + size, sizeof(name - d - size));
	}
	else if ((dp = opendir(d)))
		return (ft_close_folder(folder, dp));
	return (folder);
}
