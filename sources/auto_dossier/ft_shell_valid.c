#include <unistd.h>
#include "libft.h"
#include "ft_shell.h"

int		ft_shell_valid(int possibilities)
{
	unsigned int nbr;

	nbr = 0;

	if (possibilities > 100)
	{
		ft_putstr("\nDisplay all ");
		ft_putnbr(possibilities);
		ft_putstr(" possibilities? (y or n)");
		while (1)
		{
			if ((read(0, &nbr, sizeof(nbr))) > 0)
			{
				if (nbr == 'y')
					return (1);
				else if (nbr == ' ')
					return (1);
				else if (nbr == 'n')
					return (0);
				nbr = 0;
			}
		}
	}
	return (1);
}
