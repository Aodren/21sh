#include "ft_shell.h"
#include "ft_struct.h"
#include "libft.h"
#include <stdlib.h>

static void	ft_create_pwd_folder(char *dossier, char *name, t_21sh *sh,
		unsigned int *tab)
{
	dossier = ft_strdup(".");
	ft_handle_something(dossier, name, sh, tab);
	free(dossier);
	dossier = 0;
}

static void	ft_free_folders(char *dossier, char *new_name, t_21sh *sh)
{
	if (dossier)
		free(dossier);
	if (new_name)
		free(new_name);
	ft_free_info(sh->begin_info);
	sh->begin_info = 0;
}

static void	ft_affiche_selection_folder(unsigned int *tab, t_21sh *sh,
		char *name, char **f)
{
	unsigned short	valid;

	valid = ft_shell_valid(tab[0]);
	if (valid != 0)
	{
		if (tab[0] > 1)
		{
			ft_putchar('\n');
			ft_display_autocomplete_cmd(sh->begin_info, sh, tab[1], tab[0]);
		}
	}
	else
		ft_putchar('\n');
	if (tab[0] == 1)
	{
		if (f[1])
			ft_add_autocomplete_folder(sh, f[1], sh->begin_info->name, f[0]);
		else
			ft_add_autocomplete_folder(sh, name, sh->begin_info->name, f[0]);
	}
	else if (tab[0] > 0)
	{
		ft_putstr(sh->prompt);
		ft_display_list(sh->begin_line, 0);
	}
}

static void	ft_init_auto_dossier(char **new_name, char **dossier,
		unsigned int *tab)
{
	tab[1] = 0;
	tab[0] = 0;
	*new_name = 0;
	*dossier = 0;
}

void		ft_handle_autocomplete_dossier(char *name, t_21sh *sh)
{
	unsigned int	tab[2];
	char			*folders[2];
	char			*dossier;
	char			*new_name;



	ft_init_auto_dossier(&new_name, &dossier, tab);
	if (!name)
		ft_handle_folder_no_name(sh, &tab[1], &tab[0]);
	else
	{
		if ((dossier = ft_check_if_folder(name, &new_name)))
		{
			//if (new_name)
			//	ft_handle_something(dossier, new_name, sh, tab);
		//	else  // test sur long term
				ft_handle_something(dossier, new_name, sh, tab);
		}
		else
			ft_create_pwd_folder(dossier, name, sh, tab);
	}
	folders[0] = dossier;
	folders[1] = new_name;
	ft_affiche_selection_folder(tab, sh, name, folders);
	ft_free_folders(dossier, new_name, sh);
}
