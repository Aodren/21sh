/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_autocomplete.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/18 14:30:48 by abary             #+#    #+#             */
/*   Updated: 2016/10/23 14:13:22 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_shell.h"
#include "libft.h"
#include <stdlib.h>
#include "ft_trie.h"

static void	ft_add_autocomplete_cara(t_cmd *search, t_21sh *sh)
{
	t_cmd *tmp;

	search = search->child;
	if (sh->focus)
		sh->focus = sh->focus->next;
	tmp = search;
	while (search)
	{
		ft_add_sentence(search->letter, sh, 0);
		search = search->child;
	}
	ft_add_sentence(' ', sh, 0);
}

static void	ft_init_handle_auto(t_cmd *search, t_21sh *sh,
		size_t *size_max, unsigned int *nbr)
{
	*nbr = 0;
	*size_max = 0;
	if (search->info)
	{
		sh->begin_info = search->info;
		sh->end_info = search->info;
		*size_max = search->info->size;
		*nbr += 1;
	}
}

static void	ft_handle_autocomplete(t_cmd *search, t_21sh *sh)
{
	unsigned int	nbr;
	size_t			size_max;
	unsigned short	valid;

	if (!search)
		return ;
	ft_init_handle_auto(search, sh, &size_max, &nbr);
	ft_create_list_info(search->child, sh, &size_max, &nbr);
	valid = ft_shell_valid(nbr);
	if (valid != 0 && nbr > 1)
	{
		ft_putchar('\n');
		ft_display_autocomplete_cmd(sh->begin_info, sh, size_max, nbr);
	}
	else if (nbr > 1)
		ft_putchar('\n');
	if (nbr == 1)
		ft_add_autocomplete_cara(search, sh);
	else
	{
		ft_putstr(sh->prompt);
		ft_display_list(sh->begin_line, 0);
	}
	ft_info_null(sh->begin_info);
	sh->begin_info = 0;
}

void		ft_autocomplete(t_21sh *sh)
{
	int		ret;
	char	*name;
	t_cmd	*search;

	name = 0;
	ret = ft_check_where_i_search(sh);
	name = ft_create_search_name(sh);
	if (ret == 1)
	{
		if (name)
		{
			search = ft_search_commande(sh->cmd_root->child, name, name);
			ft_handle_autocomplete(search, sh);
		}
	}
	else
		ft_handle_autocomplete_dossier(name, sh);
	if (name)
		free(name);
}
