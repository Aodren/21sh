#include "ft_shell.h"
#include "libft.h"

/*
** Return 1 si commande , 2 si dossier
** Remplie search avec le noeud a cherhcher si commande
** Il faudrait repemser l'autocomplete avec un system de token
*/

int		ft_check_where_i_search(t_21sh *sh)
{
	t_sent *send;

	send = sh->focus != 0 ? sh->focus : sh->end;
	if (!send)
		return (1);
	else if (send->input == ' ')
	{
		while (send && ft_isspace(send->input))
			send = send->prev;
		if (!send || send->input == ';' || send->input == '|' || send->input == '&')
			return (1);
		else
			return (2);
	}
	if (send->input == '/')
		return 2;
	if (send->input == ';' || send->input == '|' || send->input == '&')
			return (1);
	while (send && !ft_isspace(send->input))
	{
		if (send->input == ';' || send->input == '|' || send->input == '&')
			return (1);
	if (send->input == '/')
		return 2;
		send = send->prev;
	}
		while (send && ft_isspace(send->input))
			send = send->prev;
	if (!send || send->input == ';' || send->input == '|' || send->input == '&')
		return (1);
	return (2);
}
