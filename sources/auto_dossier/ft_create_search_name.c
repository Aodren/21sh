#include "libft.h"
#include "ft_shell.h"
#include <stdlib.h>

char	*ft_create_search_name(t_21sh *sh)
{
	char	*name;
	char	*d;
	t_sent	*input;
	size_t size;

	size = 0;
	name = (void *)0;
	input = sh->focus != 0 ? sh->focus : sh->end;
	if (!input || input->input == ' ')
		return name;
	while (input && !ft_isspace(input->input)
			&& (input->input != '|' && input->input != '&' && input->input
				!= ';' ))
	{
		++size;
		input = input->prev;
	}
	input = input == 0 ? sh->begin_line : input->next;
	if (!size)
		return name;
	name = ft_memalloc(sizeof(char) * (size + 1));
	d = name;
	while (size > 0)
	{
		*name++ = input->input;
		input = input->next;
		--size;
	}
	return (d);
}
