#include "ft_shell.h"
#include "libft.h"
#include <stdlib.h>
#include <dirent.h>

static void	ft_add_sentence_folder_or_not(DIR *dp, t_21sh *sh)
{
	if (dp)
	{
		ft_add_sentence('/', sh, 0);
		closedir(dp);
	}
	else
		ft_add_sentence(' ', sh, 0);
}

void		ft_add_autocomplete_folder(t_21sh *sh, char *name,
		char *search, char *folder)
{
	char	*d;
	char	*check;
	DIR		*dp;

	d = search;
	dp = 0;
	if (sh->focus)
		sh->focus = sh->focus->next;
	while (name && *name && *name == *search)
	{
		++name;
		++search;
	}
	while (*search)
		ft_add_sentence(*search++, sh, 0);
	if (folder)
	{
		check = ft_strjoin(folder, d);
		dp = opendir(check);
		free(check);
	}
	else
		dp = opendir(d);
	ft_add_sentence_folder_or_not(dp, sh);
}
