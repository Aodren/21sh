#include "libft.h"
#include "ft_shell.h"
#include "ft_trie.h"

static void	ft_add_info_list(t_cmd_info *tmp, t_cmd_info *tmp2,
		t_cmd_info *new, t_21sh *sh)
{
	if (!tmp2)
	{
		new->next = sh->begin_info;
		sh->begin_info = new;
	}
	else
	{
		tmp2->next = new;
		new->next = tmp;
	}
}

void		ft_add_folder_info(t_21sh *sh, char *name, unsigned int *s_max)
{
	t_cmd_info	*tmp;
	t_cmd_info	*tmp2;
	t_cmd_info	*new;

	new = ft_init_info(name, 0);
	*s_max = new->size > *s_max ? new->size : *s_max;
	if (!sh->begin_info)
	{
		sh->begin_info = new;
		return ;
	}
	tmp = sh->begin_info;
	tmp2 = 0;
	while (tmp && ft_strcmp(tmp->name, new->name) < 0)
	{
		tmp2 = tmp;
		tmp = tmp->next;
	}
	ft_add_info_list(tmp, tmp2, new, sh);
}
