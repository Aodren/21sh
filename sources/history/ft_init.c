#include "libft.h"
#include "ft_shell.h"
#include <wchar.h>

t_history	*ft_init_history(char *str)
{
	t_history	*new;

	new = ft_memalloc(sizeof(t_21sh));
	new->prev = 0;
	new->next = 0;
	new->line = str;
	return (new);
}

char		*ft_fill_str_history(t_sent *tmp, char *str, unsigned int nbr)
{
	unsigned int	i;

	while (tmp)
	{
		if (tmp->tab)
		{
			i = 8;
			*(str + nbr++) = '\t';
			while (tmp && i)
			{
				tmp = tmp->next;
				--i;
			}
			if (!tmp)
				break ;
			else
				continue ;
		}
		*(str + nbr) = (tmp->input);
		++nbr;
		tmp = tmp->next;
	}
	*(str + nbr) = 0;
	return (str);
}

char		*ft_create_new_history(t_21sh *sh)
{
	t_history		*new;
	t_sent			*tmp;
	unsigned int	nbr;
	char			*str;

	nbr = 0;
	tmp = sh->begin;
	new = 0;
	while (tmp)
	{
		++nbr;
		tmp = tmp->next;
	}
	str = ft_memalloc(sizeof(char) * (nbr + 1));
	tmp = sh->begin;
	return (ft_fill_str_history(tmp, str, 0));
}
