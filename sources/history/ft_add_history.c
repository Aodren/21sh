#include "ft_shell.h"
#include "libft.h"

void	ft_add_history(t_21sh *sh)
{
	t_history	*new;
	char		*str;

	if (ft_check_no_empty_line(sh))
	{
		str = ft_create_new_history(sh);
		new = ft_init_history(str);
		if (!sh->begin_history)
		{
			sh->begin_history = new;
			sh->end_history = new;
		}
		else
		{
			sh->end_history->next = new;
			new->prev = sh->end_history;
			sh->end_history = new;
		}
	}
}
