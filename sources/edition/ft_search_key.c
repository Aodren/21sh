#include "ft_shell.h"

static int	ft_search_ctrl_keys(unsigned int cara)
{
	if (IS_SUP(cara))
		return (SUP);
	if (IS_CTRL_B(cara))
		return (BACKWORD);
	if (IS_CTRL_F(cara))
		return (FORWORD);
	if (IS_CTRL_P(cara))
		return (PASTE);
	if (IS_LINE_UP(cara))
		return (CTRL_H);
	if (IS_LINE_DOWN(cara))
		return (CTRL_L);
	return (NOKEY);
}

static int	ft_search_no_insert(unsigned int cara, t_21sh *sh)
{
	if (!sh->insertion)
	{
		if (IS_ENTER(cara))
			return (ENTER);
		if (IS_UP(cara))
			return (UP);
		if (IS_DOWN(cara))
			return (DOWN);
		if (IS_DELETE(cara))
			return (DELETE);
		if (IS_HOME(cara))
			return (HOME);
		if (IS_END(cara))
			return (END);
		if (IS_SYN(cara))
			return (SYN);
		if (IS_HT(cara))
			return (HT);
		return (ft_search_ctrl_keys(cara));
	}
	return (NOKEY);
}

int			ft_search_key(unsigned int cara, t_21sh *sh)
{
	int ret;

	ret = 0;
	if ((ret = ft_search_no_insert(cara, sh)) != NOKEY)
		return (ret);
	if (sh->insertion)
	{
		if (cara == 'c')
			return (COPIE);
		if (cara == 'x')
			return (COUPER);
	}
	if (IS_RIGHT(cara))
		return (RIGHT);
	if (IS_LEFT(cara))
		return (LEFT);
	if (IS_CTRL_E(cara))
		return (CTRL_E);
	if (IS_ECHAP(cara))
		return (ECHAP);
	return (NOKEY);
}
