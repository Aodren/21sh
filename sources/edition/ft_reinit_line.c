#include "ft_shell.h"
#include "libft.h"

void	ft_reinit_line(t_21sh *sh)
{
	ft_clear_sentence(sh->begin);
	sh->s_sent = 0;
	sh->begin = (void *)0;
	sh->end = (void *)0;
	sh->focus = (void *)0;
	sh->begin_line = (void *)0;
	sh->cursor_x = ft_strlen(sh->prompt);
	sh->cursor_y = 0;
	sh->ponctuations = 0;
	sh->parentheses = 0;
	sh->size_line = 0;
	sh->old_line_y = 0;
	sh->last_action = 2;
	sh->x_print = 0;
}
