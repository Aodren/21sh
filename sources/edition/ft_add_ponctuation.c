#include "ft_shell.h"
#include "libft.h"

// A revoir
// on liasse pour les tests mais va degager quand il yauar parseur /lexer
void	ft_add_ponctuation(unsigned int cara, t_21sh *sh)
{
	if (IS_DOUBLE_QUOTES(cara))
	{
		if (SINGLE_QUOTES_OK(sh->ponctuations)
				&& MAGIC_QUOTES_OK(sh->ponctuations))
			DOUBLE_QUOTES(sh->ponctuations);
	}
	if (IS_SINGLE_QUOTES(cara))
	{
		if (DOUBLE_QUOTES_OK(sh->ponctuations)
				&& MAGIC_QUOTES_OK(sh->ponctuations))
			SINGLE_QUOTES(sh->ponctuations);
	}
	if (IS_PAR_OPEN(cara))
	{
		if (SINGLE_QUOTES_OK(sh->ponctuations) &&
				DOUBLE_QUOTES_OK(sh->ponctuations))
			sh->parentheses++;
	}
	if (IS_PAR_CLOSE(cara))
	{
		if (SINGLE_QUOTES_OK(sh->ponctuations) &&
				DOUBLE_QUOTES_OK(sh->ponctuations))
			sh->parentheses--;
	}
	if (IS_MAGIC_QUOTES(cara))
	{
		if (SINGLE_QUOTES_OK(sh->ponctuations) &&
				DOUBLE_QUOTES_OK(sh->ponctuations))
			MAGIC_QUOTES(sh->ponctuations);
	}
}
