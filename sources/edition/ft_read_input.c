#include <unistd.h>
#include "ft_shell.h"
#include "libft.h"

int		ft_read_input(t_21sh *sh)
{
	unsigned int		cara;
	int					ret;
	unsigned int		y;

	cara = 0;
	if (read(0, &cara, 4) > 0)
	{
		ft_add_ponctuation(cara, sh);
		ft_calcul_pos_cursor(sh);
//		ft_print_memory(&cara, 4);
		y = sh->cursor_y;
		ret = ft_add_cara(cara, sh);
		if (ret == -1)
		{
			ft_calcul_pos_cursor(sh);
			if (sh->cursor_x == 0)
			{
				ft_termcaps_cursor_down();
				ft_termcaps_cursor_begin();
			}
		}
		return (ret);
	}
	return (NOKEY);
}
