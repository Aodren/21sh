#include <stdlib.h>
#include "ft_shell.h"
#include "libft.h"

int		ft_add_sentence_before_focus(t_21sh *sh, t_sent *new)
{
	if (sh->focus == sh->begin)
	{
		sh->begin->prev = new;
		new->next = sh->begin;
		sh->begin = new;
		sh->begin_line = new;
	}
	else
	{
		new->next = sh->focus;
		new->prev = sh->focus->prev;
		sh->focus->prev->next = new;
		sh->focus->prev = new;
		if (sh->focus == sh->begin_line)
			sh->begin_line = new;
	}
	return (ADD_CARA);
}

int		ft_add_sentence(unsigned int cara, t_21sh *sh, unsigned char tab)
{
	t_sent *new;

	if (!cara)
		return (0);
	new = ft_init_sent(cara, tab);
	if (!sh->begin_line)
		sh->begin_line = new;
	if (!sh->focus)
	{
		ft_putchar(cara);
		if (!sh->begin)
			sh->begin = new;
		else
		{
			sh->end->next = new;
			new->prev = sh->end;
		}
		sh->end = new;
	}
	else
		return (ft_add_sentence_before_focus(sh, new));
	return (0);
}

int		ft_add_cara(unsigned int cara, t_21sh *sh)
{
	int ret;

	ret = -1;
	if ((ret = ft_search_key(cara, sh)) >= 0)
		return (ret);
	if (sh->insertion)
		return (NOKEY);
	if (IS_FN(cara))
		ft_add_sentence(FN(cara), sh, 0);
	else if (IS_PAGE_DOWN(cara))
		ft_add_sentence((cara = 126), sh, 0);
	else if (IS_PAGE_UP(cara))
		ft_add_sentence((cara = 126), sh, 0);
	else if (ft_isprint(cara))
	{
		ret = 0;
		ret |= ft_add_sentence(CARA0(cara), sh, 0);
		ret |= ft_add_sentence(CARA1(cara), sh, 0);
		ret |= ft_add_sentence(CARA2(cara), sh, 0);
		ret |= ft_add_sentence(CARA3(cara), sh, 0);
		return (ret > 0 ? ret : NOKEY);
	}
	return (NOKEY);
}

void	ft_clear_sentence(t_sent *begin)
{
	if (begin)
	{
		ft_clear_sentence(begin->next);
		free(begin);
	}
}
