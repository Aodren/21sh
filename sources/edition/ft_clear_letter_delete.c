#include "libft.h"
#include "ft_shell.h"
#include <stdlib.h>

void	ft_del_first_cara(t_21sh *sh)
{
	t_sent	*sent;
	char	end;

	end = 0;
	if (sh->begin == sh->end)
		end = 1;
	if (sh->focus)
		sh->focus->prev = (void *)0;
	sent = sh->begin;
	sh->begin = sh->begin->next;
	sh->begin_line = sh->begin;
	if (end)
	{
		sh->begin = (void *)0;
		sh->end = (void *)0;
		sh->end = (void *)0;
	}
	free(sent);
}

void	ft_del_begin_line(t_21sh *sh)
{
	t_sent *sent;

	if (sh->focus)
		sh->focus->prev = (void *)0;
	sent = sh->begin_line;
	sh->begin_line = sh->begin_line->next;
	free(sent);
}

void	ft_clear_last_cara(t_21sh *sh)
{
	t_sent *sent;

	sent = sh->end;
	if (sh->end->prev)
	{
		sh->end->prev->next = (void *)0;
		sh->end = sh->end->prev;
	}
	else
	{
		sh->begin = (void *)0;
		sh->end = (void *)0;
		sh->focus = (void *)0;
	}
	free(sent);
}

void	ft_clear_mid_cara(t_21sh *sh)
{
	t_sent *delete;

	delete = sh->focus->prev;
	delete->prev->next = sh->focus;
	sh->focus->prev = delete->prev;
	free(delete);
}

int		ft_clear_letter_delete(t_21sh *sh)
{
	if (!sh->begin || !sh->begin_line || sh->begin == sh->focus
			|| sh->begin_line == sh->focus)
		return (0);
	if ((sh->focus && sh->focus->prev && !sh->focus->prev->tab)
			|| (!sh->focus && sh->end && !sh->end->tab))
	{
		if (sh->focus == sh->begin->next)
			ft_del_first_cara(sh);
		else if (sh->begin_line->next == sh->focus)
			ft_del_begin_line(sh);
		else if (!sh->focus)
			ft_clear_last_cara(sh);
		else
			ft_clear_mid_cara(sh);
	}
	else
		ft_clear_letter_delete_tab(sh);
	return (1);
}
