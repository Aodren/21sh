#include "libft.h"
#include "ft_shell.h"

void	ft_create_buffer(t_21sh *sh)
{
	t_sent *tmp;
	t_sent *new;

	tmp = sh->begin;
	if (sh->focus)
		sh->focus->highlight = 1;
	while (tmp)
	{
		if (tmp->highlight)
		{
			new = ft_init_sent(tmp->input, tmp->tab);
			if (!sh->buffer)
			{
				sh->buffer = new;
				sh->buffer_end = new;
			}
			else
			{
				sh->buffer_end->next = new;
				new->prev = sh->buffer_end;
				sh->buffer_end = new;
			}
		}
		tmp = tmp->next;
	}
}
