#include "libft.h"
#include "ft_shell.h"

static void	ft_init_pos(t_21sh *sh, unsigned int *tab)
{
	if (IS_MULTI_LINE(sh->ponctuations))
		tab[3] = 1;
	else
		tab[3] = ft_strlen(sh->prompt);
	tab[2] = 0;
	tab[0] = -1;
	tab[1] = 0;
	sh->n_presence = 0;
}

static void	ft_cursor_no_focus(t_21sh *sh, unsigned int *tab)
{
	if (tab[3] >= sh->winsize_x || (sh->end && sh->end->input == '\n'))
	{
		if (sh->end && sh->end->input == '\n')
			sh->n_presence = 1;
		tab[0] = 0;
		tab[1] = ++tab[2];
	}
	else
	{
		tab[0] = tab[3];
		tab[1] = tab[2];
	}
}

static void	ft_put_pos_cursor(t_21sh *sh, unsigned int *tab)
{
	sh->cursor_x = tab[0];
	sh->cursor_y = tab[1];
	sh->y_line = tab[2];
	sh->line = tab[2];
}

static void	ft_pos_cursor_find_focus(unsigned int *tab)
{
	tab[0] = tab[3];
	tab[1] = tab[2];
}

void		ft_calcul_pos_cursor(t_21sh *sh)
{
	t_sent			*tmp;
	t_sent			*tmp2;
	unsigned int	tab[4];

	tmp = sh->begin_line;
	tmp2 = 0;
	ft_init_pos(sh, tab);
	while (tmp)
	{
		if (tab[3] >= sh->winsize_x || (tmp2 && tmp2->input == '\n'))
		{
			tab[3] = 0;
			if (tmp2->input == '\n')
				sh->n_presence = 1;
			tab[2]++;
		}
		if (tmp == sh->focus)
			ft_pos_cursor_find_focus(tab);
		++tab[3];
		tmp2 = tmp;
		tmp = tmp->next;
	}
	if (!sh->focus)
		ft_cursor_no_focus(sh, tab);
	ft_put_pos_cursor(sh, tab);
}
