#include "ft_shell.h"

void	ft_clear_letter_delete_tab(t_21sh *sh)
{
	unsigned int y;

	y = 8;
	while (y)
	{
		if (sh->focus == sh->begin->next)
			ft_del_first_cara(sh);
		else if (sh->begin_line->next == sh->focus)
			ft_del_begin_line(sh);
		else if (!sh->focus)
			ft_clear_last_cara(sh);
		else
			ft_clear_mid_cara(sh);
		sh->cursor_x--;
		--y;
	}
}
