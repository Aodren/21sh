#include "ft_shell.h"
#include "libft.h"
#include <stdlib.h>

static void		ft_sup_first_cara(t_21sh *sh)
{
	t_sent *sent;

	sent = sh->begin;
	sh->focus->prev = 0;
	sh->begin = sh->begin->next;
	sh->focus = sh->begin;
	sh->begin_line = sh->begin;
	free(sent);
}

static void		ft_sup_cara(t_21sh *sh)
{
	t_sent *sent;

	sent = sh->focus;
	sent->prev->next = sent->next;
	sent->next->prev = sent->prev;
	sh->focus = sent->next;
	free(sent);
}

static void		ft_sup_last_cara(t_21sh *sh)
{
	sh->focus->prev->next = (void *)0;
	sh->end = sh->focus->prev;
	free(sh->focus);
	sh->focus = (void *)0;
}

static void		ft_clear_letter_sup_tab(t_21sh *sh)
{
	unsigned int y;

	y = 8;
	while (y)
	{
		if (sh->focus == sh->begin)
			ft_sup_first_cara(sh);
		else if (sh->focus == sh->end)
			ft_sup_last_cara(sh);
		else
			ft_sup_cara(sh);
		--y;
	}
}

int				ft_clear_letter_sup(t_21sh *sh)
{
	if (!sh->focus)
		return (0);
	ft_add_ponctuation(sh->focus->input, sh);
	sh->s_sent--;
	if ((sh->focus && !sh->focus->tab) ||
			(!sh->focus && sh->begin && !sh->begin->tab))
	{
		if (sh->focus == sh->begin)
			ft_sup_first_cara(sh);
		else if (sh->focus == sh->end)
			ft_sup_last_cara(sh);
		else
			ft_sup_cara(sh);
	}
	else
		ft_clear_letter_sup_tab(sh);
	return (1);
}
