#include "libft.h"
#include "ft_shell.h"

int	ft_check_no_empty_line(t_21sh *sh)
{
	t_sent	*tmp;

	tmp = sh->begin;
	while (tmp)
	{
		if (ft_isgraph(tmp->input))
			return (1);
		tmp = tmp->next;
	}
	return (0);
}

int			ft_check_line(t_21sh *sh)
{
	(void )sh;

	/*
	if (!QUOTES_OK(sh->ponctuations))
		return (QUOTES);
	if (sh->parentheses)
		return (QUOTES);
	if (ft_check_no_empty_line(sh))
	{
		if (sh->end->input == ('\\'))
			return (QUOTES);
		return (SINGLE_LINE);
	}
	else
	*/
		return (EMPTY_LINE);
}
