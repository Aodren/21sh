#include "ft_shell.h"
#include <stdlib.h>
#include <term.h>
#include "libft.h"

void	ft_init_access_db(void)
{
	char	*env_term;
	int		access_termdb;
	int		alloc;

	env_term = (void *)0;
	alloc = 0;
	env_term = getenv("TERM");
	if (!env_term && (alloc = 1))
		env_term = ft_strdup("vt100");
	access_termdb = tgetent((void *)0, env_term);
	if (alloc)
		free(env_term);
	if (access_termdb < 0)
		ft_display_error(DATABASE_FOUND);
	if (access_termdb == 0)
		ft_display_error(UNDEFINED_TERM);
}
