#include "libft.h"
#include "ft_shell.h"
#include <term.h>

void	ft_termcaps_init_space(void)
{
	int ret;

	ret = tgetnum("it");
}

void	ft_termcaps_clear_tab_stop(void)
{
	char *ret;

	if (!(ret = tgetstr("st", (void *)0)))
		return ;
	tputs(ret, 0, ft_putchar_term);
}
