#include "ft_shell.h"
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

static char	*ft_check_tty(void)
{
	if (!isatty(0))
		return ((void *)0);
	return (ttyname(0));
}

void		ft_open_tty(t_21sh *sh)
{
	char	*ret;

	ret = (void *)0;
	if (!(ret = ft_check_tty()))
		ft_display_error(TTY);
	if ((sh->fd = open(ret, O_RDWR)) == -1)
		ft_display_error(TTY);
}
