#include "ft_shell.h"
#include <term.h>

void	ft_termcaps_highlight_on(void)
{
	char *ret;

	if ((ret = tgetstr("so", (void *)0)))
		tputs(ret, 0, ft_putchar_term);
}

void	ft_termcaps_highlight_off(void)
{
	char *ret;

	if ((ret = tgetstr("se", (void *)0)))
		tputs(ret, 0, ft_putchar_term);
}
