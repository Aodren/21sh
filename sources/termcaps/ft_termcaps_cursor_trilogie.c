#include "ft_shell.h"
#include <term.h>

void	ft_termcaps_old_position(void)
{
	char *ret;

	if (!(ret = tgetstr("rc", 0)))
		return ;
	tputs(ret, 0, ft_putchar_term);
}

void	ft_termcaps_cursor_nright(unsigned int nbr)
{
	char *ret;

	if (!(ret = tgetstr("ch", (void *)0)))
		return ;
	tputs(tgoto(ret, 0, nbr), 0, ft_putchar_term);
}

void	ft_termcaps_cursor_ndown(unsigned int nbr)
{
	char *ret;

	if (!(ret = tgetstr("ch", (void *)0)))
		return ;
	tputs(tgoto(ret, 0, nbr), 0, ft_putchar_term);
}
