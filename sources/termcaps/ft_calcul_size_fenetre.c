#include <sys/ioctl.h>
#include "ft_shell.h"

void	ft_get_size_fenetre(t_21sh *sh)
{
	struct winsize win_size;

	if ((ioctl(0, TIOCGWINSZ, &win_size)) != -1)
	{
		sh->winsize_y = win_size.ws_row;
		sh->winsize_x = win_size.ws_col;
	}
}
