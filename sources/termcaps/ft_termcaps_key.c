#include "ft_shell.h"
#include "libft.h"
#include <term.h>

void	ft_termcap_key_home(void)
{
	char *ret;

	if (!(ret = tgetstr("k5", (void *)0)))
		return ;
	ft_print_memory(ret, sizeof(ret));
}
