#include "ft_shell.h"
#include <term.h>

void	ft_termcaps_clear_line(void)
{
	char *ret;

	if (!(ret = tgetstr("ce", (void *)0)))
		return ;
	tputs(ret, 0, ft_putchar_term);
}

void	ft_termcaps_clear_down(void)
{
	char *ret;

	if (!(ret = tgetstr("cd", (void *)0)))
		return ;
	tputs(ret, 0, ft_putchar_term);
}
