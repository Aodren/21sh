#include "ft_shell.h"
#include <term.h>

void	ft_termcaps_move_cursor(unsigned int col, unsigned int line)
{
	char *ret;

	if (!(ret = tgetstr("cm", (void *)0)))
		return ;
	tputs(tgoto(ret, col , line), 0, ft_putchar_term);
}

void	ft_termcaps_cursor_down(void)
{
	char	*ret;

	if (!(ret = tgetstr("do", (void *)0)))
		return ;
	tputs(ret, 0, ft_putchar_term);
}

void	ft_termcaps_cursor_up(void)
{
	char	*ret;

	if (!(ret = tgetstr("up", (void *)0)))
		return ;
	tputs(ret, 0, ft_putchar_term);
}

void	ft_termcaps_cursor_invisible(void)
{
	char *ret;

	if (!(ret = tgetstr("vi", (void *)0)))
		return ;
	tputs(ret, 0, ft_putchar_term);
}

void	ft_termcaps_cursor_visible(void)
{
	char *ret;

	if (!(ret = tgetstr("ve", (void *)0)))
		return ;
	tputs(ret, 0, ft_putchar_term);
}
