#include <sys/ioctl.h>
#include "ft_shell.h"
#include <termios.h>

void	ft_get_old_term(t_21sh *sh)
{
	int ret;

	ret = tcgetattr(0, &sh->old_termios);
	if (ret == -1)
		ft_display_error(TERMIOS_ERROR);
}

void	ft_set_new_term(t_21sh *sh)
{
	int ret;

	ret = tcgetattr(0, &sh->new_termios);
	if (ret == -1)
		ft_display_error(TERMIOS_ERROR);
	sh->new_termios.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP
		| INLCR | IGNCR | ICRNL | IXON);
	sh->new_termios.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN);
	sh->new_termios.c_cflag &= ~(CSIZE | PARENB);
	sh->new_termios.c_cflag |= CS8;
	sh->new_termios.c_cc[VMIN] = 0;
	sh->new_termios.c_cc[VTIME] = 0;
	ret = tcsetattr(sh->fd, TCSANOW, &sh->new_termios);
	if (ret == -1)
		ft_display_error(TERMIOS_ERROR);
}

void	ft_set_old_term(t_21sh *sh)
{
	int ret;

	ret = tcsetattr(0, TCSANOW, &sh->old_termios);
	if (ret == -1)
		ft_display_error(TERMIOS_ERROR);
}
