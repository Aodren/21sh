#include "ft_shell.h"
#include <term.h>

void	ft_termcaps_cursor_begin(void)
{
	char *ret;

	if (!(ret = tgetstr("cr", (void *)0)))
		return ;
	tputs(ret, 0, ft_putchar_term);
}

void	ft_termcaps_cursor_left(void)
{
	char *ret;

	if (!(ret = tgetstr("le", (void *)0)))
		return ;
	tputs(ret, 0, ft_putchar_term);
}

void	ft_termcaps_cursor_right(void)
{
	char *ret;

	if (!(ret = tgetstr("nd", (void *)0)))
		return ;
	tputs(ret, 0, ft_putchar_term);
}

void	ft_termcaps_save_position(void)
{
	char *ret;

	if (!(ret = tgetstr("sc", 0)))
		return ;
	tputs(ret, 0, ft_putchar_term);
}
