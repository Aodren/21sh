#include "ft_shell.h"
#include <stdlib.h>
#include <unistd.h>
#include "ft_trie.h"

void	ft_destruct_shell(t_21sh *sh)
{
	ft_set_old_term(sh);
	close(sh->fd);
	ft_free_env(sh->env);
	ft_free_path(sh->begin_path);
	ft_free_trie(sh->cmd_root);
	free(sh->lexer);
	if (sh->prompt)
		free(sh->prompt);
	free(sh);
}
