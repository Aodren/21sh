#include "libft.h"
#include "ft_shell.h"
#include <stdlib.h>
#include "ft_actions.h"

static void	ft_init_actions(t_21sh *sh)
{
	sh->actions[ESC] = (void *)0;
	sh->actions[DOWN] = ft_sh_down;
	sh->actions[UP] = ft_sh_up;
	sh->actions[LEFT] = ft_sh_left;
	sh->actions[RIGHT] = ft_sh_right;
	sh->actions[HOME] = ft_sh_home;
	sh->actions[END] = ft_sh_end;
	sh->actions[DELETE] = ft_sh_delete_cara;
	sh->actions[HT] = ft_sh_tab;
	sh->actions[SYN] = ft_sh_ctrl_v;
	sh->actions[ENTER] = ft_sh_enter;
	sh->actions[ADD_CARA] = ft_sh_add_cara;
	sh->actions[SUP] = ft_sh_sup_cara;
	sh->actions[BACKWORD] = ft_sh_backword;
	sh->actions[FORWORD] = ft_sh_forword;
	sh->actions[CTRL_E] = ft_sh_ctrl_i;
	sh->actions[COPIE] = ft_action_copie_buffer;
	sh->actions[COUPER] = ft_action_couper_buffer;
	sh->actions[PASTE] = ft_sh_paste;
	sh->actions[ECHAP] = ft_sh_echap;
	sh->actions[CTRL_H] = ft_action_ctrl_h;
	sh->actions[CTRL_L] = ft_action_ctrl_l;
}

static void	ft_init_enter(t_21sh *sh)
{
	sh->enter[SINGLE_LINE] = ft_enter_single_line;
	sh->enter[EMPTY_LINE] = ft_enter_empty_line;
	sh->enter[MULTI_LINES] = ft_enter_multi_lines;
}

static void	ft_init_prompt(t_21sh *sh, char *prompt)
{
	if (sh->prompt)
	{
		free(sh->prompt);
		sh->prompt = (void *)0;
	}
	sh->prompt = ft_strdup(prompt);
}

#include "ft_lexer.h"
t_21sh		*ft_init_shell(t_21sh *sh)
{
	t_lexer *lexer;

	lexer = 0;
	if (!(sh = (t_21sh *)ft_memalloc(sizeof(t_21sh))))
		ft_display_error(MALLOC);
	ft_bzero(sh, sizeof(t_21sh));
	sh->fd = -1;
	sh->last_action = 2;
	ft_get_size_fenetre(sh);
	ft_open_tty(sh);
	ft_init_access_db();
	ft_get_old_term(sh);
	ft_set_new_term(sh);
	ft_init_actions(sh);
	ft_init_enter(sh);
	ft_init_prompt(sh, "21sh > ");
	sh->lexer = ft_init_lexer(lexer);
	sh->cursor_x = ft_strlen(sh->prompt);
	sh->size_line = ft_strlen(sh->prompt);
	return (sh);
}

t_sent		*ft_init_sent(unsigned int cara, unsigned char tab)
{
	t_sent *sent;

	if (!(sent = (t_sent *)ft_memalloc(sizeof(t_sent))))
		ft_display_error(MALLOC);
	sent->input = cara;
	sent->next = (void *)0;
	sent->operateur = 0;
	sent->prev = (void *)0;
	sent->highlight = 0;
	sent->tab = tab;
	return (sent);
}
