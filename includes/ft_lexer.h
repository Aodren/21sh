#ifndef FT_LEXER_H
# define FT_LEXER_H

#include "ft_struct.h"



// voir peut etre pour un ordre judicieux
// pour l'instant on s'arrete a trois fonctions 
// voir ou on part
// foutre peut etre CMD et arg a la fin

int				ft_lexer(t_21sh *sh);
t_lexer			*ft_init_lexer(t_lexer *lexer);
t_token			*ft_init_token(t_token *token, t_lexer *lexer);

void			ft_display_token(t_token *token);
int				ft_lexer_single_quotes(t_token *token , t_lexer *lexer,
		t_21sh *sh);
/*
 ** Fonctions a passer sur la value du token une fois creer
 */

//char			*ft_echapment(char *value);
void			ft_lexer_variable(t_token *token, t_lexer *lexer, t_21sh *sh);

void			ft_malloc_value(t_lexer *lexer, t_token *token, unsigned int size);
int				ft_is_echapement(int c);
int				ft_lexer_double_quotes(t_token *token, t_lexer *lexer,
		t_21sh *sh);


int				ft_lexer_strdupequ(t_token *token, t_lexer *lexer
		, int c);

t_sent			*ft_lstpbrk(t_sent *begin, const char *str2, unsigned int *size);

void			ft_shell_home(t_token *token, t_21sh *sh, t_lexer *lexer);
/*
 ** fonction d'analyse des tokens
 */

int			ft_lexer_cmd(t_21sh *sh, t_token *token, t_lexer *lexer);
int			ft_lexer_arg(t_21sh *sh, t_token *token, t_lexer *lexer);
int			ft_lexer_value(t_21sh *sh, t_token *token, t_lexer *lexer);

int			ft_lexer_comment(t_21sh *sh, t_token *token, t_lexer *lexer);

int			ft_lexer_equal(t_21sh *sh, t_token *token, t_lexer *lexer);
int			ft_lexer_and(t_21sh *sh, t_token *token, t_lexer *lexer);
int			ft_lexer_background(t_21sh *sh, t_token *token, t_lexer *lexer);
int			ft_lexer_pipe(t_21sh *sh, t_token *token, t_lexer *lexer);
int			ft_lexer_or(t_21sh *sh, t_token *token, t_lexer *lexer);
int			ft_lexer_popen(t_21sh *sh, t_token *token, t_lexer *lexer);
int			ft_lexer_pclose(t_21sh *sh, t_token *token, t_lexer *lexer);
int			ft_lexer_mquotes(t_21sh *sh, t_token *token, t_lexer *lexer);
int			ft_lexer_end(t_21sh *sh, t_token *token, t_lexer *lexer);
int			ft_lexer_dquotes(t_21sh *sh, t_token *token, t_lexer *lexer);
int			ft_lexer_files(t_21sh *sh, t_token *token, t_lexer *lexer);


int			ft_lexer_more_than(t_21sh *sh, t_token *token, t_lexer *lexer);
int			ft_lexer_less_than(t_21sh *sh, t_token *token, t_lexer *lexer);
int			ft_lexer_whitespaces(t_21sh *sh, t_token *token, t_lexer *lexer);
int			ft_lexer_agregation(t_21sh *sh, t_token *token, t_lexer *lexer);
int			ft_lexer_(t_21sh *sh, t_token *token, t_lexer *lexer);
int			ft_lexer_files(t_21sh *sh, t_token *token, t_lexer *lexer);
int			ft_lexer_backslash(t_21sh *sh, t_token *token, t_lexer *lexer);

int			ft_lexer_open_brackets(t_21sh *sh, t_token *token, t_lexer *lexer);
int			ft_lexer_close_brackets(t_21sh *sh, t_token *token, t_lexer *lexer);



int			ft_lexer_d_less_than(t_21sh *sh, t_token *token, t_lexer *lexer);
int			ft_lexer_d_more_than(t_21sh *sh, t_token *token, t_lexer *lexer);
int			ft_lexer_end_of_files(t_21sh *sh, t_token *token, t_lexer *lexer);
/*
** Destruct 
*/

void		ft_free_tokens(t_token *begin);	
#endif
