#ifndef FT_HISTORY_H
#define FT_HISTORY_H

t_history	*ft_init_history(char *str);
void		ft_add_history(t_21sh *sh);
char		*ft_create_new_history(t_21sh *sh);
void		ft_put_history(t_21sh *sh);
#endif
