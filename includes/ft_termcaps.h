#include "ft_struct.h"
/*
*******************************************************************************
**			TERMIOS
*******************************************************************************
*/

void	ft_get_old_term(t_21sh *sh);
void	ft_set_new_term(t_21sh *sh);
void	ft_set_old_term(t_21sh *sh);

/*
*******************************************************************************
**		TERMCAPS
*******************************************************************************
*/
int		ft_putchar_term(int c);
void	ft_termcap_key_home(void);
/*
**		CLEAR
*/
void	ft_termcaps_clear_line(void);

/*
**		CURSOR
*/
void	ft_termcaps_cursor_begin(void);
void	ft_termcaps_cursor_left(void);
void	ft_termcaps_cursor_right(void);
void	ft_termcaps_cursor_nright(unsigned int nbr);
void	ft_termcaps_cursor_ndown(unsigned int nbr);
void	ft_termcaps_save_position(void);
void	ft_termcaps_old_position(void);
void	ft_termcaps_highlight_on(void);
void	ft_termcaps_highlight_off(void);
void	ft_termcaps_cursor_invisible(void);
void	ft_termcqps_cursor_visible(void);
void	ft_termcaps_cursor_down(void);
void	ft_termcaps_cursor_up(void);
void	ft_termcaps_clear_down(void);
void	ft_termcaps_move_cursor(unsigned int col, unsigned int line);
void	ft_termcaps_init_space();
void	ft_termcaps_clear_tab_stop();
