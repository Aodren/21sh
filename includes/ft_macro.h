#ifndef FT_MACRO_h
#define FT_MACRO_h

#define HISTORY_MAX 10
/*
**	Marco pou savoir si c'est une touche fn
*/

#define IS_FN(x) (x & 0x00FFFF00) == 0x00315b00 ? 1 : 0
#define FN(x) (x & 0xFF000000) >> 16 >> 8

/*
** Macro pour savoir si c'ets un senmble de cara imprimable
** Dans ce cas le premier cacaractere est un cara different de 0
**
*/

#define IS_MULTIPLE(x) (x & 0x000000FF) >= 0x20 ? 1 : 0

/*
**	Macro qui renvoie le caractere voulu
*/

#define CARA0(x) (x & 0x000000FF)
#define CARA1(x) (x & 0x0000FF00) >> 8
#define CARA2(x) (x & 0x00FF0000) >> 16
#define CARA3(x) (x & 0xFF000000) >> 16 >> 8


/*
**	MACRO KEY
*/

#define IS_UP(x) (x ^ 0x00415b1b) ? 0 : 1
#define IS_DOWN(x) (x ^ 0x00425b1b) ? 0 : 1
#define IS_LEFT(x) (x ^ 0x00445b1b) ? 0 : 1
#define IS_RIGHT(x) (x ^ 0x00435b1b) ? 0 : 1
#define IS_PAGE_DOWN(x) (x ^ 0x7e365b1b) ? 0 : 1
#define IS_PAGE_UP(x) (x ^ 0x7e355b1b) ? 0 : 1
#define IS_DELETE(x) (x ^ 0x0000007f) ? 0 : 1
#define IS_HOME(x) (x ^ 0x00485b1b) ? 0 : 1
#define IS_END(x) (x ^ 0x00465b1b) ? 0 : 1
#define IS_ENTER(x) (x == 13) ? 1 : 0
#define IS_SYN(x) (x == 0x16) ? 1 : 0
#define IS_HT(x) (x == 0x9) ? 1 : 0
#define IS_SUP(x) (x ^ 0x7e335b1b) ? 0 : 1
#define IS_CTRL_B(x) (x ^ 0x00000002) ? 0 : 1
#define IS_CTRL_F(x) (x ^ 0x00000006) ? 0 : 1
#define IS_CTRL_P(x) (x == 16) ? 1 : 0
#define IS_CTRL_E(x) (x ^ 0x5) ? 0 : 1
#define IS_ECHAP(x) (x == 27) ? 1 : 0
#define IS_LINE_UP(x) (x ^ 0x08) ? 0 : 1
#define IS_LINE_DOWN(x) (x ^ 0x0c) ? 0 : 1 /*ctlr + h*/
/*
** Ponctuation
*/

#define IS_PAR_OPEN(x) (x == 40) ? 1 : 0
#define IS_PAR_CLOSE(x) (x == 41) ? 1 : 0
#define IS_DOUBLE_QUOTES(x) (x == 34) ? 1 : 0
#define IS_SINGLE_QUOTES(x) (x == 39) ? 1 : 0
#define IS_MAGIC_QUOTES(x) (x == 96) ? 1 : 0
#define IS_BACKSLASH(x) (x == 92) ? 1 :0
#define IS_MULTI_LINE(x) (x & 0b1000) ? 1 : 0

#define SINGLE_QUOTES(x) x = (x ^ 0b1)
#define DOUBLE_QUOTES(x) x = (x ^ 0b10)
#define MAGIC_QUOTES(x) x = (x ^ 0b100)

#define SINGLE_QUOTES_OK(x) (x & 0b1) ? 0 : 1
#define DOUBLE_QUOTES_OK(x) ( x & 0b10) ? 0 : 1
#define MAGIC_QUOTES_OK(x) ( x & 0b100) ? 0 : 1
#define MULTI_LINES(x) x = (x | 0b1000)
#define QUOTES_OK(x) (x & 0b111) ? 0 : 1

/*
** operateur
*/

#define IS_CTRL_V(x) (x & 0b1) ? 1 : 0
#define IS_POINT_VIRGULE(x) (x == 0x3b)
#define IS_PIPE(x) (x == 0x7c) ? 1 : 0

#define CTRL_V(x) x = (x ^ 0b1)
#define SERACH_TRIE(x) x = (x ^ 0b10)
#define NEW_COMMANDE(x) x = (x ^ 0b100)
enum e_error_sh  {MALLOC, TERMIOS_ERROR, TTY, ENV_TERM, DATABASE_FOUND,
UNDEFINED_TERM};

#define NOKEY -1
//#define NBR_ORDRES 9
enum e_key_sh {ESC, DOWN, UP, LEFT, RIGHT, HOME, END, DELETE, ENTER,
ADD_CARA, SYN, HT, SUP, FORWORD, BACKWORD,CTRL_E, ECHAP, COPIE, COUPER, PASTE,
CTRL_H, CTRL_L, NBR_ORDRES};

//enum e_line_sh {SINGLE_LINE, PARSE_ERROR, QUOTES, BACKSLASH, EMPTY_LINE, NBR_ENTER};

enum e_line_sh {SINGLE_LINE, MULTI_LINES, EMPTY_LINE, NBR_ENTER};

// Ajouter newline

enum e_token {NONE,	WHITESPACES, O_BRACKETS, C_BRACKETS,
	COMMENT, D_MORE_THAN, D_LESS_THAN, LESS_THAN,
	AGREGATION, MORE_THAN, T_BACKSLASH,
	AND, BACKGROUND, OR, PIPE, POPEN, PCLOSE, MQUOTES,EQUAL,
	WORD, 
   	END_OF_STATEMENT, // indique la fin des fonctions 
	NBR_TOKEN , NVARIABLE ,CMD, VALUE, ARG, T_FILE, END_OF_LINE, END_OF_FILE, AMBIGOUS};

#endif
