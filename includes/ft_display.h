#ifndef FT_DISPLAY_H
#define FT_DISPLAY_H

/*
*******************************************************************************
**									DISPLAY
******************************************************************************
*/
void	ft_display_list(t_sent *sent, int actions);
void	ft_display_autocomplete_cmd(t_cmd_info *info, t_21sh *sh, size_t size,
		unsigned int nbr);
void	ft_display_line(t_21sh *sh);
void	ft_display_reput_line(t_21sh *sh, int actions);
void	ft_display_history(unsigned int y, t_21sh *sh);
#endif
