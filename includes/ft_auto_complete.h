#ifndef FT_AUTO_COMPLETE_H
#define FT_AUTO_COMPLETE_H
#include "ft_struct.h"

void	ft_autocomplete(t_21sh *sh);
int		ft_check_where_i_search(t_21sh *sh);
void	ft_handle_autocomplete_dossier(char *name, t_21sh *sh);
//char	*ft_search_folder(t_21sh *sh);
void	ft_info_null(t_cmd_info *begin);
void	ft_free_info(t_cmd_info *info);
char	*ft_check_if_folder(char *name, char **new_name);
char	*ft_create_search_name(t_21sh *sh);
void	ft_add_autocomplete_folder(t_21sh *sh, char *name, char *search,
	   	char *folder);
void	ft_handle_something(char *folder, char *name, t_21sh *sh,
		unsigned int *tab);
void	ft_handle_folder_no_name(t_21sh *sh,
		unsigned int *size_max, unsigned int *nbr);
void	ft_add_folder_info(t_21sh *sh, char *name, unsigned int *s_max);


#endif
