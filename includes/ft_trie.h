#ifndef FT_TRIE_H
#define FT_TRIE_H

#include "ft_struct.h"
#include <dirent.h>

t_cmd	*ft_init_cmd_root(void);
void	ft_add_trie_node(t_cmd *root, char *path, char *name, char *nom);
void	ft_create_trie(t_21sh *sh, struct dirent *dp, char *path);
void	ft_free_trie(t_cmd *root);
t_cmd_info	*ft_init_info(char *name, char *path);
void		ft_create_list_info(t_cmd *root, t_21sh *sh, size_t *s_max,
		unsigned int *nbr);
void	ft_no_child(t_cmd *cmd, char *path, char *name, char *nom);
t_cmd_info	*ft_init_info(char *name, char *path);

#endif
