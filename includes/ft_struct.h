#ifndef FT_STRUCT_H
#define FT_STRUCT_H
#include "ft_macro.h"
#include <termios.h>
#include <sys/types.h>


typedef struct s_21sh t_21sh;

typedef struct	s_token
{
	int 		token; // type du token cf enum token
	unsigned int size;
	char		*value ; // commande variable, argument ................
	struct s_token *next;
	struct s_token *prev; // a voir si utile ?? (ambiguite)
}				t_token;

#define NBR_REGEX 1
typedef struct s_lexer
{
	unsigned int state;

	struct s_sent			*focus; // la ou on en est dans le parcours de la ligne
	char					**regex; // inutile
	int			(*f_tokens[NBR_TOKEN])(t_21sh *sh, t_token *token, 
			struct s_lexer *lexer); // 8 pour l'instant
	char		open_brackets;//useless
	int			cmd;
	t_token		*begin;
	t_token		*end;
}				t_lexer;

/*
** Information sur le CMD
*/

typedef struct	s_cmd_info
{
	char			*name; // nom de la commande
	char			*path; // path de la commande
	size_t			size;
	struct s_cmd_info	*next;
}					t_cmd_info;

/*
** Arbre Pour l'autocompletion des commandes
*/

typedef struct	s_cmd
{
	struct	s_cmd *child;// node first child
	struct	s_cmd *next; //parent node's next child
	char	letter;
	t_cmd_info	*info; // pointeur verrs l'info
}		t_cmd;

/*
** Recupere les touches appuyer au clavier
*/

typedef struct	s_sent
{
	unsigned int	input;
	unsigned char	operateur; // contient un termial// pre check n'est
	unsigned char	tab;
	char			highlight;
	struct s_sent	*next;
	struct s_sent	*prev;
}				t_sent;

/*
** Contient les differents elements du path
*/

typedef struct s_path
{
	char			*path;
	struct s_path	 	*next;
}				t_path;

/*
**	liste chaine de l'historique"
*/

#include <wchar.h>
typedef struct	s_history
{
	char		*line;
	struct s_history *next;
	struct s_history *prev;
	/*
	t_sent			*begin;
	t_sent			*end;
	*/
	/*
	struct s_history *next;
	struct s_history *prev;
	*/
}		t_history;

typedef struct	s_21sh
{
	struct termios	old_termios;
	struct termios	new_termios;
	t_sent			*begin;
	t_sent			*end;
	t_sent			*focus; // important
	t_sent			*begin_line;
	t_sent			*focus_insertion;
	t_sent			*buffer;
	t_sent			*buffer_end;
	unsigned int		size_line;
	short			last_action;
	/* Calcul de la position du cursor */
	unsigned int		old_line_y;
	unsigned int		n_presence;
	unsigned int		cursor_x;
	unsigned int		cursor_y;
	unsigned int		y_line;
	unsigned int		x_line; // nbr de caractere de la ligne
	unsigned int		x_print; // nbr de caractere visible
	unsigned int		line;
	unsigned int		s_sent; // taille de la ligne courante
	t_cmd			*cmd_root; // racinde du Trie

	int			(*actions[NBR_ORDRES])(struct s_21sh*);
	int			(*enter[NBR_ENTER])(struct s_21sh*); // utile ?????
	int			fd;
	unsigned char		parse;
	unsigned char	parse_error;
	unsigned char	ponctuations;
	int		parentheses; // 0 pour ok
	unsigned int	size_env; // taille du tableau 2d de l'env
	unsigned short	winsize_x;
	unsigned short	winsize_y;
	char			*prompt;


	char			**v_locales; /// variables locales
	char			**env; // l'envirronement
	t_cmd_info		*begin_info; 
	t_cmd_info		*end_info; // pas utile?
	t_path			*begin_path;
	t_path			*end_path;


	t_lexer			*lexer;
	t_history		*begin_history;
	t_history		*end_history; // on  va le garder pour free le derner maillon
	t_history		*focus_history;

	unsigned int		nbr_history;




	char			*path;// plutot faire une liste chaine de variables internes??
	char			insertion;
	short			delete_end;//test
}				t_21sh;


#endif
