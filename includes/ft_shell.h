#ifndef FT_SHELL_H
# define FT_SHELL_H
#include <termios.h>
#include <string.h>
#include "ft_struct.h"
#include "ft_auto_complete.h"
#include "ft_display.h"
#include "ft_history.h"
#include "ft_actions.h"
#include "ft_termcaps.h"



/*
*******************************************************************************
**				INIT
*******************************************************************************
*/
t_21sh	*ft_init_shell(t_21sh *sh);
void	ft_open_tty(t_21sh *sh);
void	ft_init_access_db(void);
t_sent	*ft_init_sent(unsigned int cara, unsigned char tab);
void	ft_reinit_line(t_21sh *sh);

/*
*******************************************************************************
**				SENTENCE
*******************************************************************************
*/
int		ft_add_cara(unsigned int cara, t_21sh *sh);
int		ft_add_sentence(unsigned int cara, t_21sh *sh, unsigned char tab);
int		ft_add_sentence_before_focus(t_21sh *sh, t_sent *new);
int		ft_search_key(unsigned int cara, t_21sh *sh);
void	ft_create_buffer(t_21sh *sh);


/*
*******************************************************************************
**									21sh
*******************************************************************************
*/

void	ft_21sh(t_21sh *sh);
int		ft_read_input(t_21sh *sh);

/*
*******************************************************************************
**									Tools
*******************************************************************************
*/

char		*ft_get_value_variable(t_21sh *sh, char *variable);
/*
 * Modif/add/suppresion env
 */
char	*ft_getenv(char *var, char **env);
void	ft_create_env(t_21sh *sh, char **env);
void	ft_add_env(t_21sh *sh, char *var);

/*
 * Gestion du PATH
 */
void	ft_init_path(t_21sh *sh);
void	ft_add_path(t_21sh *sh, char *path, size_t size);
void	ft_recup_path(t_21sh *sh);

/*
 * Read dir
 */
void	ft_read_dir_trie(t_21sh *sh, char *dir);


/*
 * size fenetre
 */
void	ft_get_size_fenetre(t_21sh *sh);
/*
 * Trie
 */
#include <dirent.h>
t_cmd	*ft_init_node(char *name, char *path, char *nom);
t_cmd	*ft_search_commande(t_cmd *root, char *name, char *nom);
void	ft_parcour_trie(t_cmd *root);


// Il faut une autcompletion sur les cmds/builtin /
// les dossiers, gerer le rezise de la fenetre -> fonction d'affichage
// il vas falloir gerer avec les ; && ||
/*
******************************************************************************
**				CHECK
******************************************************************************
*/
void	ft_add_ponctuation(unsigned int cara, t_21sh *sh);
int		ft_check_line(t_21sh *sh);
int		ft_check_no_empty_line(t_21sh *sh);
int		ft_shell_valid(int possibilities);
void	ft_calcul_pos_cursor(t_21sh *sh);
/*
*******************************************************************************
**									SIGNAL
*******************************************************************************
*/


/*
*******************************************************************************
**									ERROR
*******************************************************************************
*/
void	ft_display_error(int error);

/*
*******************************************************************************
**								DEBUG
*******************************************************************************
*/
void	ft_test_reverse(t_sent *sent);
/*
*******************************************************************************
**									DESTRUCT
*******************************************************************************
*/
void	ft_destruct_shell(t_21sh *sh);
void	ft_free_path(t_path *path);
void	ft_free_env(char **env);
void	ft_clear_sentence(t_sent *sent);
int		ft_clear_letter_delete(t_21sh *sh);
void	ft_clear_letter_delete_tab(t_21sh *sh);

int		ft_clear_letter_sup(t_21sh *sh);
void	ft_del_first_cara(t_21sh *sh);
void	ft_del_begin_line(t_21sh *sh);
void	ft_clear_last_cara(t_21sh *sh);
void	ft_clear_mid_cara(t_21sh *sh);

void	ft_reinir_line(t_21sh *sh);
#endif
