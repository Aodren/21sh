#ifndef FT_ACTIONS_H
#define FT_ACTIONS_H

/*
**	Actions
*/

int		ft_sh_right(t_21sh *sh);
int		ft_sh_left(t_21sh *sh);
int		ft_sh_enter(t_21sh *sh);
int		ft_sh_add_cara(t_21sh *sh);
int		ft_sh_delete_cara(t_21sh *sh);
int		ft_sh_down(t_21sh *sh);
int		ft_sh_up(t_21sh *sh);
int		ft_sh_home(t_21sh *sh);
int		ft_sh_end(t_21sh *sh);
int		ft_sh_tab(t_21sh *sh);
int		ft_sh_ctrl_v(t_21sh *sh);
int		ft_sh_sup_cara(t_21sh *sh);
int		ft_sh_backword(t_21sh *sh);
int		ft_sh_forword(t_21sh *sh);
int		ft_sh_ctrl_i(t_21sh *sh);
int		ft_sh_echap(t_21sh *sh);
int		ft_sh_copier(t_21sh *sh);
int		ft_action_copie_buffer(t_21sh *sh);
int		ft_action_couper_buffer(t_21sh *sh);
int		ft_action_ctrl_h(t_21sh *sh);
int		ft_action_ctrl_l(t_21sh *sh);

int		ft_sh_paste(t_21sh *sh);

/*
**	Enter
*/

int		ft_enter_single_line(t_21sh *sh);
int		ft_enter_multi_lines(t_21sh *sh);
int		ft_enter_empty_line(t_21sh *sh);

/*
**	Tools
*/

t_sent	*ft_focus_up(t_sent *sent, unsigned int cursor_y,
		t_21sh *sh, int action);
#endif
